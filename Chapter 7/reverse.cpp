#include <iostream>

int main ()
{
  std::string word;
  getline(std::cin, word);
  for (std::string::iterator it = word.end();it != word.begin() - 1;--it)
    std::cout << *it << std::endl;
  return 0;
}
