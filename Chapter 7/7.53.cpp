#include <iostream>

class Debug
{
public:
  constexpr Debug (bool b = true): hw(b), io (b), other (b) {}
  constexpr Debug (bool h, bool i, bool o): hw (h), io (i), other (o) {}
  void setHw (bool i)
  {
    hw = i;
  }
  void setIo (bool i)
  {
    io = i;
  }
  void setOther (bool i)
  {
    other = i;
  }
private:
    bool hw;
    bool io;
    bool other;
};

int main ()
{
  return 0;
}
