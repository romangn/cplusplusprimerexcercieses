#include <iostream>

void doStuff (const int &a, const int &b)
{
  a = 5;
  b = 10;
}

int main ()
{
  int a = 1, b = 2;
  std::cout << a << " " << b << std::endl;
  doStuff(a, b);
  std::cout << a << " " << b << std::endl;
  std::string i = "first", j = "second";
  std::string& ref = j;
  std::cout << ref << std::endl;
  ref = i;
  std::cout << i << j << std::endl;
  return 0;
}
