#include <iostream>

struct X
{
  X (int i, int j) : base (i), rem (i % j) {}
  X (std::istream &is = std::cin)
  {
    is >> word;
  }
  X (std::string a = "") : word (a) {}
  int base, rem;
  std::string word;
};

int main ()
{
  X *object  = new X ();
  std::cout << object->word << std::endl;
  return 0;
}
