#include <iostream>

struct Sales_data
{
  std::string isbn () const
  {
    return bookNo;
  }
  Sales_data &combine (const Sales_data &data)
  {
    booksSold += data.booksSold;
    revenue += data.revenue;
    return *this;
  }

  std::string bookNo;
  unsigned int booksSold = 0;
  double revenue = 0.0;
};

Sales_data add (const Sales_data &data1, const Sales_data &data2)
{
  Sales_data sum = data1;
  sum.combine(data2);
  return sum;
}

std::istream &read (std::istream &is, Sales_data &item)
{
  double price = 0.0;
  is >> item.bookNo >> item.booksSold >> price;
  item.revenue = (price * item.booksSold);
  return is;
}

std::ostream &print (std::ostream &os, const Sales_data &item)
{
  os << item.isbn() << " " << item.booksSold << " " << item.revenue << std::endl;
  return os;
}

int main ()
{
  Sales_data total;
  if (read(std::cin, total))
  {
    Sales_data trans;
    while (read(std::cin, trans))
    {
      if (total.isbn() == trans.isbn())
        total.combine(trans);
      else
      {
        print(std::cout, total);
        total = trans;
      }
    }
    print (std::cout, total);
  }
  else
  {
    std::cerr << "No data" << std::endl;
  }
  return 0;
}
