#include <iostream>
#include <vector>

class NoDefault
{
public:
  NoDefault (int i) {}
};

class C
{
public:
  C () : def (0) {}
  NoDefault def;
};

int main ()
{

  std::vector<C> vec (10);
  return 0;
}
