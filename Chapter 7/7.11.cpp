#include <iostream>

struct Sales_data
{
  Sales_data () = default;
  Sales_data (std::string name, int volume, double profit)
  {
    bookNo = name;
    booksSold = volume;
    revenue = profit * volume;
  }
  Sales_data (std::istream &is)
  {
    read (is, *this);
  }
  std::string isbn () const
  {
    return bookNo;
  }
  std::istream &read (std::istream &is, Sales_data &data){
    is >> data.bookNo >> data.booksSold >> data.revenue;
    return is;
  }

  Sales_data &combine (const Sales_data &data)
  {
    booksSold += data.booksSold;
    revenue += data.revenue;
    return *this;
  }

  std::string bookNo;
  unsigned int booksSold = 0;
  double revenue = 0.0;
};

std::ostream &print (std::ostream &os, Sales_data &data)
{
  os << data.bookNo << " " << data.booksSold << " " << data.revenue << std::endl;
  return os;
}

int main ()
{
  Sales_data total (std::cin);
  if (!total.isbn().empty())
  {
    std::istream &is = std::cin;
    while (is)
    {
      Sales_data trans (is);
      if (trans.isbn() == total.isbn())
        total.combine(trans);
      else
      {
        print(std::cout, total);
        total = trans;
      }
    }
    print(std::cout, total);
  }
  else
  {
    std::cerr << "No data" << std::endl;
    return -1;
  }
  return 0;
}
