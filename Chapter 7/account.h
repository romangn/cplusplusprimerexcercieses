#include <iostream>

class Account
{
public:
  Account (std::string name, int num, double d): accName (name),
    accNumber(num), balance (d){}
  Account () : Account ("", 0, 0.0) {}
  Account (std::istream &is)
  {
    read (is, *this);
  }
  Account &read (std::istream &is, Account &acc)
  {
    is >> acc.accName >> acc.accNumber >> acc.balance;
    return *this;
  }
private:
  std::string accName;
  unsigned int accNumber = 0;
  double balance = 0.0;
  static double rate;
};
