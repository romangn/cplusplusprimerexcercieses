#include <iostream>
struct Sales_data
{
  std::string isbn () const
  {
    return bookNo;
  }
  Sales_data &combine (const Sales_data &data)
  {
    booksSold += data.booksSold;
    revenue += data.revenue;
    return *this;
  }
  std::string bookNo;
  unsigned int booksSold = 0;
  double revenue = 0;
};

int main ()
{
  Sales_data total;
  if (std::cin >> total.bookNo >> total.booksSold >> total.revenue)
  {
    Sales_data trans;
    while (std::cin >> trans.bookNo >> trans.booksSold >> trans.revenue)
    {
      if (total.isbn() == trans.isbn())
      {
        total.combine(trans);
      }
      else
      {
        std::cout << total.booksSold <<" " << total.revenue << std::endl;
        total = trans;
      }
    }
    std::cout << total.booksSold << " " << total.revenue << std::endl;
  }
  else
  {
    std::cerr << "No data" << std::endl;
    return -1;
  }
  return 0;
}
