#include <iostream>
#include "screen.h"

std::ostream &print (std::ostream &os, const Screen &screen)
{
  os << screen.getHeight() << " " << screen.getWidth() << " " << screen.getContents() << std::endl;
  return os;
}

int main ()
{
  Screen screen (5, 5, 'X');
  screen.move(4, 2).set('#').display(std::cout);
  screen.display(std::cout);
  return 0;
}
