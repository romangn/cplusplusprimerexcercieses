#include <iostream>

class Account
{
public:
  Account (std::string name, int num, double d): accName (name),
    accNumber(num), balance (d){}
  Account () : Account ("", 0, 0.0) {}
  Account (std::istream &is)
  {
    read (is, *this);
  }
  static void initRate (double r);
  std::string getName () const
  {
    return accName;
  }
  int getAccNumber () const
  {
    return accNumber;
  }
  double getBalance () const
  {
    return balance;
  }
  static double getCurrentRate ()
  {
    return rate;
  }
  std::istream &read (std::istream &is, Account &acc)
  {
    is >> acc.accName >> acc.accNumber >> acc.balance;
    return is;
  }
  Account &addFunds (double d)
  {
    balance += d;
    return *this;
  }

  Account *withdraw (double d)
  {
    balance -= d;
    return this;
  }

  Account &changeName (std::string name)
  {
    accName = name;
    return *this;
  }

  std::ostream &print (std::ostream &os, Account &acc);

private:
  std::string accName;
  unsigned int accNumber = 0;
  double balance = 0.0;
  static double rate;
};
double Account::rate;

void Account::initRate (double r)
{
  rate = r;
}

std::ostream &print (std::ostream &os, Account &acc)
{
  os << acc.getName() << " " << acc.getAccNumber() << " " <<
    acc.getBalance() << std::endl;
  return os;
}

int main ()
{
  Account::initRate(5.0);
  Account::initRate(50.0);
  Account acc (std::cin);
  std::cout << Account::getCurrentRate() << std::endl;
  print(std::cout, acc);
  acc.addFunds(500.0);
  print(std::cout, acc);
  acc.withdraw(300.0);
  acc.changeName("Chris");
  print(std::cout, acc);
  return 0;
}
