#include <iostream>
struct Person
{
  std::string getName () const
  {
    return name;
  }
  std::string getAddress () const
  {
    return address;
  }
  std::string name;
  std::string address;
};

  std::istream &read (std::istream &is,Person &person)
  {
    is >> person.name >> person.address;
    return is;
  }

  std::ostream &print (std::ostream &os, Person &person)
  {
    os << person.name << person.address << std::endl;
    return os;
  }

int main ()
{
  Person guy;
  read(std::cin, guy);
  print(std::cout, guy);
  return 0;
}
