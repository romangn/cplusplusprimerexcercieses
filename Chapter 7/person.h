#include <iostream>

class Person
{
  friend std::ostream &print (std::ostream &os, const Person &person);
  public:
  Person () = default;
  Person (std::string n, std::string a)
  {
    name = n;
    address = a;
  }
  Person (std::istream &is)
  {
    read (is, *this);
  }
  std::string getName () const
  {
    return name;
  }
  std::string getAddress () const
  {
    return address;
  }
  private:
  std::istream &read (std::istream &is, Person &person)
  {
    is >> person.name >> person.address;
    return is;
  }
  std::string name;
  std::string address;
};
std::ostream &print (std::ostream &os, const Person &person);
