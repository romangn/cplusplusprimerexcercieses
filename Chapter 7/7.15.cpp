#include <iostream>

struct Person
{
  Person () = default;
  Person (std::istream &is)
  {
    read(is, *this);
  }
  Person (std::string n, std::string a)
  {
    name = n;
    address = a;
  }
  std::string getName () const
  {
    return name;
  }
  std::string getAddress () const
  {
    return address;
  }
  std::istream &read (std::istream &is, Person &person)
  {
    is >> person.name >> person.address;
    return is;
  }
  std::string name;
  std::string address;
};

std::ostream &print (std::ostream &os, Person &person)
{
  os << person.name << " " << person.address << std::endl;
  return os;
}

int main ()
{
  Person jack (std::cin);
  print (std::cout , jack);
  return 0;
}
