#include <iostream>

struct Sales_data
{
  std::string isbn;
  unsigned int booksSold = 0;
  double revenue = 0.0;
};

int main ()
{
  Sales_data total;
  if (std::cin >> total.isbn >> total.booksSold >> total.revenue)
  {
    Sales_data trans;
    while (std::cin >> trans.isbn >> trans.booksSold >> trans.revenue)
    {
      if (trans.isbn == total.isbn)
      {
          total.booksSold += trans.booksSold;
          total.revenue += trans.revenue;
      }
      else
      {
        std::cout << total.booksSold << " " << total.revenue << std::endl;
        total = trans;
      }
    }
    std::cout << total.booksSold << " " << total.revenue << std::endl;
  }
  else
  {
    std::cerr << "No data" << std::endl;
    return - 1;
  }
  return 0;
}
