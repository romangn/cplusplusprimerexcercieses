#include <iostream>

class Sales_data
{
  friend std::ostream &print (std::ostream &os, const Sales_data &data);
  public:
  Sales_data (std::string name, int volume, double profit): bookNo(name), booksSold (volume),
    revenue(profit){std::cout << "Initialize all three members" << std::endl;}
  Sales_data () : Sales_data ("", 0, 0.0) {std::cout << "Default construtor" <<std::endl;}
  Sales_data (std::istream &is): Sales_data ()
  {
    read (is, *this);
    std::cout << "Doing the write" << std::endl;
  }
  std::string isbn () const
  {
    return bookNo;
  }
  Sales_data &combine (const Sales_data &data)
  {
    booksSold += data.booksSold;
    revenue += data.revenue;
    return *this;
  }
  private:
    std::istream &read (std::istream &is, Sales_data &data)
    {
      is >> data.bookNo >> data.booksSold >> data.revenue;
      return is;
    }
    std::string bookNo;
    unsigned int booksSold = 0;
    double revenue = 0.0;
};
std::ostream &print (std::ostream &os, const Sales_data &data);
