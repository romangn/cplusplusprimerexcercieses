#include <iostream>
#include "SalesData.h"

std::ostream &print (std::ostream &os, const Sales_data &data)
{
  os << data.bookNo << " " << data.booksSold << " " << data.revenue << std::endl;
  return os;
}

int main ()
{
  Sales_data total (std::cin);
  if (!total.isbn().empty())
  {
    std::istream &is = std::cin;
    while (is)
    {
      Sales_data trans (is);
      if (trans.isbn() == total.isbn())
        total.combine(trans);
      else
      {
        print(std::cout, total);
        total = trans;
      }
    }
    print (std::cout, total);
  }
  else
  {
      std::cerr << "No data" << std::endl;
      return -1;
  }
  return 0;
}
