#include <iostream>
#include <vector>

class Screen;
class WindowManager
{
  public:
    using theScreen = std::vector<Screen>::size_type;
    WindowManager () = default;
    void clear (theScreen scr);
  private:
    std::vector<Screen> screens;
};

class Screen
{
  friend void WindowManager::clear (theScreen scr);
  public:
  using pos = std::string::size_type;
  Screen () = default;
  Screen (pos ht, pos wid)
  {
    contents = ' ' * (ht + wid);
  }
  Screen (pos ht, pos wid, char c)
  {
    height = ht;
    width = wid;
    contents = c;
  }
  pos getHeight () const
  {
    return height;
  }
  pos getWidth () const
  {
    return width;
  }
  std::string getContents () const
  {
    return contents;
  }
  Screen &set (char c)
  {
    contents += c;
    return *this;
  }
  Screen &move (pos r, pos col)
  {
    int i = 0;
    while (i < r * col)
    {
      contents += ' ';
      ++i;
    }
    return *this;
  }
  Screen &display (std::ostream &os)
  {
    doDisplay(os);
    return *this;
  }
  const Screen &display (std::ostream &os) const
  {
    doDisplay(os);
    return *this;
  }
  std::ostream &print (std::ostream &is, const Screen &screen);
  pos size () const;
  private:
  void doDisplay (std::ostream &os) const
  {
    os << contents << std::endl;
  }
  pos height = 0;
  pos width = 0;
  std::string contents;
};

Screen::pos Screen::size () const
{
  return height * width;
}
