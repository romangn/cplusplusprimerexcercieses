#include <iostream>
#include <stdexcept>

class Employee
{
public:
Employee (std::string n, unsigned int i, std::string p, double s): name (n),
  id (i), position (p), salary (s) {}
Employee () : Employee ("", 0, "", 0.0) {}
Employee (std::istream &is): Employee ()
{
  is >> name >> id >> position >> salary;
}
std::string getName () const
{
  return name;
}
int getId () const
{
  return id;
}
std::string getPosition () const
{
  return position;
}
double getSalary () const
{
  return salary;
}
Employee &addSalary (double d)
{
  salary += d;
  return *this;
}
Employee &substractSalary (double d)
{
  if ((salary - d) < 0)
    throw std::invalid_argument ("error");
  salary -= d;
  return *this;
}
private:
std::string name;
unsigned int id = 0;
std::string position;
double salary = 0.0;
};

std::ostream &print (std::ostream &os, Employee &employee)
{
  os << employee.getName()<< " " << employee.getId() << " " << employee.getPosition() <<
    " " << employee.getSalary() << std::endl;
  return os;
}
int main ()
{
  Employee employee ;
  employee.addSalary(500.0);
  employee.substractSalary(1100.0);
  print (std::cout, employee);
  return 0;
}
