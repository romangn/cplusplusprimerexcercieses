// 10.30.cpp : Defines the entry point for the console application.
// AND 10.31

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>


int main()
{
	std::vector<int> input;
	std::istream_iterator<int> in(std::cin);
	std::istream_iterator<int> eof;
	while (in != eof)
	{
		input.push_back(*in++);
	}
	std::sort(input.begin(), input.end());
	std::unique_copy(input.begin(), input.end(), std::ostream_iterator<int>(std::cout, " "));
    return 0;
}

