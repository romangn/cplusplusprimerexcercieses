// 10.29.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <iterator>
#include <fstream>


int main()
{
	std::vector<std::string> Text;
	std::ifstream in("ReadMe.txt");
	std::istream_iterator<std::string> strIter(in);
	std::istream_iterator<std::string> eof;
	while (strIter != eof) { Text.push_back(*strIter++); }
	for (auto st : Text) { std::cout << st << std::endl; }
    return 0;
}

