// 10.22.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <string>
#include <algorithm>
#include <functional>

using namespace std::placeholders;

bool checkSize(const std::string& s, std::string::size_type sz);

int main()
{
	std::vector<std::string> Words = { "First", "Test", "tester", "dog", "country", "soldier" };
	auto wc = std::count_if(Words.begin(), Words.end(), std::bind(checkSize, _1, 6));
	std::cout << wc << std::endl;
    return 0;
}

bool checkSize(const std::string& s, std::string::size_type sz)
{
	return s.size() > sz;
}

