// 10.34-10.35.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <iterator>


int main()
{
	std::vector<int> items = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
	// 10.34
	for (auto it = items.crbegin(); it != items.crend(); ++it) { std::cout << *it << std::endl; }
	// 10.35
	std::cout << "Now for the normal iterators" << std::endl;
	for (auto it = items.cend(); it != items.cbegin();)
	{
		std::cout << *--it << std::endl;
	}
    return 0;
}

