// 11.9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <map>
#include <list>
#include <vector>
#include <set>

bool compareItems(int first, int second);

int main()
{
	// 11.9
	std::map<std::list<std::string>, size_t> words;
	// 11.10
	// Can be defined
	std::map<std::vector<int>::iterator, int> first;
	std::map<std::list<int>::iterator, int> second;

	// Works normally
	std::vector<int> v1;
	first.insert(std::pair<std::vector<int>::iterator, int>(v1.begin(), 0));

	// Cant compare elements in the list
	std::list<int> l1;
	//second.insert(std::pair<std::list<int>::iterator, int>(l1.begin(), 0));

	// 11.11
	using Less = bool(*) (int first, int second);
	std::multiset<int, Less> itemstore(compareItems);
    return 0;
}

bool compareItems(int first, int second)
{
	return first < second;
}
