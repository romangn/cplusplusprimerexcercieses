// 12.10-12.13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <memory>

void process(std::shared_ptr<int> p);

int main()
{
	// 12.10 Correct
	std::shared_ptr<int> p(new int(42));
	std::cout << *p << "and " << p.use_count() << std::endl;
	process(std::shared_ptr<int>(p));
	std::cout << *p << "and " << p.use_count() << std::endl;


	// 12.11 process(shared_ptr<int>(p.get())) will create a new temporary shared pointer from the original and will pass it.
	//process(std::shared_ptr<int>(p.get()));
	//std::cout << *p << std::endl;
	// As it points to the same memory, it will get deleted as the function goes out of scope making the original p pointer dangling

	// 12.12 a) We pass a copy of shared ptr to function and it will work properly
	// b) It will not compile as there is no implicit conversion from built in pointer to smart pointer
	// c) It will attempt to pass built in pointer which will not be accepted due to lac of implicit conversion
	// d) It will attempt to create another temporary shared pointer from the built in pointer and when the function ends it will free itself
	// deleting this memory thus making the original built in pointer invalid
	auto t = new int(32);
	auto sp = std::make_shared<int>();
	process(std::shared_ptr<int>(t));
	std::cout << *t << "and " << p.use_count() << std::endl;
	// 12.13 The shared pointer will become invalid after its original memory is deleted from the built in pointer

	// General smart pointers advices: 
	// � Don�t use the same built-in pointer value to initialize (or reset) more than one smart pointer.
	// as it will affect multiple smart pointers resulting in deletion of data
	// � Don�t delete the pointer returned from get(). To avoid data being deleted twice
	// � Don�t use get() to initialize or reset another smart pointer. As when the function goes out of scope it will delete data
	// from any other smart pointers having this data
    return 0;
}

void process(std::shared_ptr<int> p)
{
	std::cout << *p << "and " << p.use_count() << std::endl;
}
