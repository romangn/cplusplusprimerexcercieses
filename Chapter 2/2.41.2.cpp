#include <iostream>
#include <string>

struct Sales_item
{
    std::string bookNo;
    int booksSold = 0;
    double revenue = 0.0;
};

int main()
{
    int counter;
    Sales_item item1, item2;
    if(std::cin >> item1.bookNo >> item1.booksSold
        >> item1.revenue)
        {
            while(std::cin >> item2.bookNo >> item2.booksSold
                 >> item2.revenue)
                 {
                     if(item2.bookNo == item1.bookNo)
                        counter++;
                     else
                        break;
                 }
             std::cout << counter << std::endl;
        }
    return 0;
}
