#include <iostream>

int main()
{
    int i = 30;
    int *p = &i;
    if (p)
        std::cout << p << std::endl;
    if (*p)
        std::cout << *p << std::endl;
    return 0;
}
