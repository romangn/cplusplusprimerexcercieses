#include "Sales_data.h"

int main()
{
    int counter = 0;
    Sales_data item1, item2;
    if(std::cin >> item1.bookNo >> item1.units_sold
        >> item1.revenue)
        {
            while(std::cin >> item2.bookNo >> item2.units_sold
                >> item2.revenue)
                {
                    if(item2.bookNo == item1.bookNo)
                        counter++;
                    else
                        break;
                }
            std::cout << counter << std::endl;
        }
    return 0;
}
