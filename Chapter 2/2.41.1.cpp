#include <iostream>
#include <string>

struct Sales_item{
    std::string bookNo;
    int booksSold = 0;
    double revenue = 0.0;
};

int main()
{
    int total = 0;
    int counter = 0;
    Sales_item item1, item2;
    std::cin >> item1.bookNo >> item2.bookNo;
    if(item1.bookNo != item2.bookNo)
    {
        std::cerr << "Not the same number" << std::endl;
        return -1;
    }
    while(counter < 5)
    {
    std::cin >> item1.booksSold >> item2.booksSold;
    std::cin >> item1.revenue >> item2.revenue;
    total += (item1.booksSold * item1.revenue) +
        + (item2.booksSold * item2.revenue);
    counter++;
    }
    std::cout << "The total is " << total << std::endl;
    return 0;
}
