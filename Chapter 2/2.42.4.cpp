#include "Sales_data.h"

int main()
{
    Sales_data item1;
    int total;
    if(std::cin >> item1.bookNo >> item1.units_sold
        >> item1.revenue)
        {
            Sales_data item2;
            while(std::cin >> item2.bookNo
                >> item2.units_sold >> item2.revenue)
                {
                    if(item1.bookNo == item2.bookNo)
                        total += (item1.units_sold * item1.revenue) +
                            (item2.units_sold * item2.revenue);
                    else
                    {
                        std::cout << total << std::endl;
                        total = item2.units_sold * item2.revenue;
                    }
                }
                std::cout << total << std::endl;
        }
    return 0;
}
