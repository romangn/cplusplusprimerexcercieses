#include "Sales_data.h"

int main()
{
    Sales_data item1;
    int total = 0;
    while(std::cin >> item1.bookNo >> item1.units_sold
        >> item1.revenue)
        {
            if(item1.bookNo == "stop")
                break;
            else
                total += item1.units_sold * item1.revenue;
        }
    std::cout << total << std::endl;
    return 0;
}
