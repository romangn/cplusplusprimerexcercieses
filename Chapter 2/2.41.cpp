#include <iostream>
#include <string>

struct Sales_data {
    std::string bookNo;
    int booksSold = 0;
    double revenue = 0.0;
};

int main()
{
    Sales_data item1;
    Sales_data item2;
    std::cin >> item1.bookNo >> item2.bookNo;
    if(item1.bookNo != item2.bookNo)
    {
        std::cout << "Different books!";
        return -1;
    }
    std::cin >> item1.booksSold >> item2.booksSold;
    std::cin >> item1.revenue >> item2.revenue;
    std::cout << (item1.booksSold * item1.revenue) +
        + (item2.booksSold * item2.revenue) << std::endl;
    return 0;
}
