// 11.27-11.30.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <string>
#include <map>


int main()
{
	// 11. 27
	// I would use count to find how many times does the item value exist in the collection or if the item exists in there at all.
	// I would use find to find the first occurance of an item or to check if it exists within the collection

	// 11.28
	std::map<std::string, std::vector<int>> theMap;
	std::map<std::string, std::vector<int>>::iterator it = theMap.find("Ted");

	// 11.29
	// All of them return the iterator to the first element larger than the searched item where it is possible to insert new item without
	// disruption of the order

	// 11.30 equal_range returns the pair of iterators. The iterator also contains a pair - key and value. The second methods
	// accesses its value
    return 0;
}

