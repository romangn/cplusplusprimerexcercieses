// 12.1-12.5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "StrBlob.h"


int main()
{
	// 12. 1 The first strblob will have 4 elements after assignment (the element being pushed back to the second container
	// still affects first strblob as they have a shared pointer now
	// and the second strblob will have 0 elements as it will be destroyed by the end by going out of scope

	// 12.2
	StrBlob str1({"test"});
	std::cout << str1.front() << std::endl;
	StrBlob str2({ "a", "ab", "abc" });
	str1 = str2;
	std::cout << str1.front() << " and " << str2.front() << std::endl;

	// 12.3 No it does not need const versions of push_back and pop_back because const functions dont modify anything
	// while in our programme the functions perform actual modifications

	// 12.4 We dont check if i is greater than zero because it checks if i is larger than size of vector and vector's size
	// cannit be less than 0, therefere it is redundant. In addition to this the type of sizeType is unsigned, therefore it
	// cannot be < 0

	// 12.5 By not using the explicit constructor we allow us the danger of potential implicit conversion of arguments from provided
	// to the strings. No particular advantages
		

    return 0;
}

