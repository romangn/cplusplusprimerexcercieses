#include "stdafx.h"
#include "StrBlobPtr.h"

std::shared_ptr<std::vector<std::string>>StrBlobPtr::check(std::size_t s, const std::string str) const
{
	auto ret = wptr.lock();
	if (!ret)
		throw std::runtime_error("unbound StrBlobPtr");
	if (s >= ret->size())
		throw std::out_of_range(str);
	return ret;
}

std::string& StrBlobPtr::deref() const
{
	auto p = check(curr, "Dereference past the end");
	return (*p)[curr];
}

StrBlobPtr& StrBlobPtr::increm()
{
	check(curr, "Dereference past the end of str blob");
	++curr;
	return *this;
}