#pragma once
#include <vector>
#include <memory>
#include <string>
#include "StrBlob.h"

class StrBlobPtr
{
public:
	StrBlobPtr() : curr(0) {};
	StrBlobPtr(StrBlob& blob, size_t size = 0) : wptr(blob.data), curr(size) {};
	
	std::string& deref() const;
	StrBlobPtr& increm();
	
private:
	std::shared_ptr <std::vector<std::string>> check(std::size_t, const std::string) const;
	std::weak_ptr<std::vector<std::string>> wptr;
	std::size_t curr;
};