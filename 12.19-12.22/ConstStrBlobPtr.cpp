#include "stdafx.h"
#include "ConstStrBlobPtr.h"

std::shared_ptr<std::vector<std::string>>ConstStrBlobPtr::check(std::size_t s, const std::string str) const
{
	auto ret = wptr.lock();
	if (!ret)
		throw std::runtime_error("unbound StrBlobPtr");
	if (s >= ret->size())
		throw std::out_of_range(str);
	return ret;
}

std::string& ConstStrBlobPtr::deref() const
{
	auto p = check(curr, "Dereference past the end");
	return (*p)[curr];
}

ConstStrBlobPtr& ConstStrBlobPtr::increm()
{
	check(curr, "Dereference past the end of str blob");
	++curr;
	return *this;
}