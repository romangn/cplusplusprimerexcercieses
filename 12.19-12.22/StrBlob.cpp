#include "stdafx.h"
#include "StrBlob.h"
#include "StrBlobPtr.h"

StrBlob::StrBlob() : data(std::make_shared<std::vector<std::string>>()) {};
StrBlob::StrBlob(std::initializer_list<std::string> li) : data(std::make_shared<std::vector<std::string>>(li)) {};

StrBlob::sizeType StrBlob::size() const
{
	return data->size();
}

bool StrBlob::empty() const
{
	return data->empty();
}

void StrBlob::check(StrBlob::sizeType i, const std::string& message) const
{
	if (i >= data->size())
		throw std::out_of_range(message);
}

void StrBlob::pushBack(const std::string& t)
{
	data->push_back(t);
}

void StrBlob::popBack()
{
	check(0, "popBack on empty StrBlob");
	data->pop_back();
}

std::string& StrBlob::back() const
{
	check(0, "back on empty StrBlob");
	return data->back();
}

std::string& StrBlob::front() const
{
	check(0, "front on empty StrBlob");
	return data->front();
}

StrBlobPtr StrBlob::begin()
{
	return StrBlobPtr(*this);
}

StrBlobPtr StrBlob::end()
{
	auto ret = StrBlobPtr(*this, data->size());
	return ret;
}





