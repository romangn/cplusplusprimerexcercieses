#pragma once
#include <vector>
#include <string>
#include <memory>

class StrBlobPtr;
class ConstStrBlobPtr;
class  StrBlob
{
public:
	friend class StrBlobPtr;
	friend class ConstStrBlobPtr;
	typedef std::vector<std::string>::size_type sizeType;
	StrBlob();
	StrBlob(std::initializer_list<std::string> li);
	sizeType size() const;
	bool empty() const;
	void pushBack(const std::string& s);
	void popBack();
	std::string& back() const;
	std::string& front() const;
	StrBlobPtr begin();
	StrBlobPtr end();
private:
	std::shared_ptr<std::vector<std::string>> data;
	void check(sizeType i, const std::string& message) const;
};

