// 12.19-12.22.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include "StrBlob.h"
#include "StrBlobPtr.h"
#include "ConstStrBlobPtr.h"
#include <fstream>


int main()
{
	// 12.19
	//StrBlob str1({ "a", "ab", "abc", "abcd"});
	//StrBlobPtr testPtr(str1);
	//size_t i = 0;
	//while (i <= str1.size())
	//{
	//	std::cout << testPtr.deref() << std::endl;
	//	i++;
	//	testPtr.increm();
	//}
	

	
	// 12.20
	StrBlob str2({});
	std::ifstream file;
	file.open("input.txt");
	std::string s = "";
	while (std::getline(file, s))
	{
		str2.pushBack(s);
	}
	file.close();
	StrBlobPtr testPtr2(str2);
	size_t r = 0;
	while (r <= str2.size())
	{
		std::cout << testPtr2.deref() << std::endl;
		r++;
		testPtr2.increm();
	}

	// 12.21 Both of these are the same but the original one is a bit more clear to read

	// 12.22
	const StrBlob bl1({"1", "2", "3", "4"});
	ConstStrBlobPtr constStrBlobPtr(bl1);

    return 0;
}

