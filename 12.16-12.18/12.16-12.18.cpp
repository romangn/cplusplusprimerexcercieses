// 12.16-12.18.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <memory>


int main()
{
	// 12.16
	std::unique_ptr<int>t(new int(42));
	// Cannot be referenced, deleted function std::unique_ptr<int>p2(p1);
	// Attempting to reference a deleted function std::unique_ptr<int>p3 = p1;

	// 12.17
	int ix = 1024, *pi = &ix, *pi2 = new int(2048);
	typedef std::unique_ptr<int> IntP;
	// a) IntP p0(ix); Cannot construct from non pointer type
	IntP p1(pi); // b) The object is not created using new therefore when it goes out of scope and gets deleted and
	// error will be thrown
	IntP p2(pi2); // c) Legal, but an object will be deleted when it goes out of scope causing pi to become dangling pointer
	IntP p3(&ix); // d) The object is not created using new therefore when it goes out of scope and gets deleted and
	// error will be thrown
	IntP p4(new int(2048)); // Fine, creates a pointer with the new value
	IntP p5(p2.get()); // If it gets deleted, will create a danglin pointer for p2 as they both point to the same data,
	// will also cause a doble delete when p2 goes out of scope

	// 12.18 Shared ptr does not have a release member because there may be other shared pointers pointing to the
	// same object. It would be useless to do a release and do operations on the aquired pointer as it will still
	// be deleted by the other shared pointers unless its unique


    return 0;
}

