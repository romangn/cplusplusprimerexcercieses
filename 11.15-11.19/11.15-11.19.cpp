// 11.15-11.19.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <algorithm>
#include <iterator>

bool compareItems(int i1, int i2);

int main()
{
	// 11. 15 map<int, vector<int>> mapped type - vector<int>, key_type - int, value_type - int, vector<int>
	// 11.16
	std::map<int, int> theMap = {{1, 0}};
	auto it = theMap.begin();
	it->second = 1;
	for (auto it : theMap) { std::cout << it.first << " " << it.second << std::endl; }

	// 11.17
	std::multiset<std::string> c;
	std::vector<std::string> v;
	std::copy(v.begin(), v.end(), std::inserter(c , c.end())); // Copies elements from vector to multiset through the inserter
	// std::copy(v.begin(), v.end(), std::back_inserter(c)); Cannot be done since there is no push back in multiset because its not sequential
	std::copy(c.begin(), c.end(), std::inserter(v, v.end())); // Copies elements from multiset to vector through the inserter
	std::copy(c.begin(), c.end(), std::back_inserter(v)); // Copies elements from multiset to vector through the back inserter
	// This is allowed here as vector allows acces to the end of the collection as vector is sequential and there is defined end

	// 11.18
	std::map<std::string, std::string>::const_iterator iter;

	// 11.19 
	std::multiset<int, decltype(compareItems)*>bookstore(compareItems);
	std::multiset<int, decltype(compareItems)*>::iterator theItems = bookstore.begin();
}

bool compareItems(int i1, int i2)
{
	return i1 < i2;
}

