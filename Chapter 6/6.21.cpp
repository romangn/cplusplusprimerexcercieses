#include <iostream>

int giveResult (int value , int* anotherValue)
{
  int result;
  if (value > *anotherValue)
    result = value;
  else if (*anotherValue > value)
    result = *anotherValue;
  return result;
}

int main ()
{
  int value = 25;
  int newValue = 10;
  int *pointer = &newValue;
  std::cout << giveResult(value, pointer) << std::endl;
  return 0;
}
