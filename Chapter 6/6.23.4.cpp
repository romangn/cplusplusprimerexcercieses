#include <iostream>

void print (int (&arr) [2][2])
{
  for (auto it = arr; it != std::end (arr);++it)
    for (auto jt = *it; jt != std::end (*it); ++jt)
      std::cout << *jt << std::endl;
}

int main ()
{
  int i [2][2] = {
    {1, 2},
    {3, 4}
  };
  print(i);
  return 0;
}
