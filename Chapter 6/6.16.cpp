#include <iostream>
#include <string>

bool is_empty (const std::string& s)
{
  return s.empty();
}

int main ()
{
  std::cout << is_empty("") << std::endl;
  return 0;
}
