#include <iostream>

void print (int (&arr) [2])
{
  for (const int elem: arr)
    std::cout << elem << std::endl;
}

int main ()
{
  int i[2] = {5, 2};
  print(i);
  return 0;
}
