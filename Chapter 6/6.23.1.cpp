#include <iostream>

void print(const char* a, int b)
{
  std::cout << b << std::endl;
  if (a)
    {
      while (*a)
        std::cout << *a++ << std::endl;
    }
}

int main ()
{
  int i = 0;
  char j[2] = {'a', 'b'};
  print (j, i);
  return 0;
}
