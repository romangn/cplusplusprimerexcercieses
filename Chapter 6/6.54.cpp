#include <iostream>
#include <vector>

int func (int a, int b);
using PF = int (*) (int, int);
std::vector<PF>collection;
int add (int a, int b)
{
  return a + b;
}

int substract (int a, int b)
{
  return a - b;
}

int multiply (int a, int b)
{
    return a * b;
}

int divide (int a, int b)
{
  return a / b;
}

int main ()
{
  collection.push_back(add);
  collection.push_back(substract);
  collection.push_back(multiply);
  collection.push_back(divide);
  for (auto it: collection)
    std::cout << it (10, 2) << std::endl;
  return 0;
}
