#include <iostream>

void print (const int ia[], size_t size)
{
  for (int i = 0; i < size;++i)
    std::cout << ia[i] << std::endl;
}

int main ()
{
  int j[2] = {1, 2};
  print (j, 2);
  return 0;
}
