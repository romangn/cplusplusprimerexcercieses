#include <iostream>
#include <vector>

int calculation (const std::vector<int> &collection, int index)
{
  if (index < collection.size())
  {
    std::cout << collection[index] << std::endl;
    return calculation(collection, index + 1);
  }
  return collection[collection.size()];
}

int main ()
{
  const std::vector<int> collection = {5, 4, 3, 2, 1};
  calculation(collection, 0);
  return 0;
}
