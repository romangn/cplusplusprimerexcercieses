#include <iostream>

void swap (int* a, int* b)
{
  int temp = *a;
  *a = *b;
  *b = temp;
}

int main ()
{
  int a = 25, b = 50;
  int* first = &a;
  int* second = &b;
  swap (first, second);
  std::cout << a << " and " << b << std::endl;
  return 0;
}
