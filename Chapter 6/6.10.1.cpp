#include <iostream>

void swap (int &a, int &b)
{
  int temp = a;
  a = b;
  b = temp;
}

int main ()
{
  int a, b;
  std::cin >> a >> b;
  std::cout << "The original a value is " << a << ". The original b value is " <<
    b << std::endl;
  int &first = a;
  int &second = b;
  swap (first, second);
  std::cout << "The new a is " << a << ". The new b is " << b << std::endl;
  return 0;
}
