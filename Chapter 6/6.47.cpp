#include <iostream>
#include <vector>

#define NDEBUG

void printVector (std::vector<int>& collection, int value)
{
  #ifdef NDEBUG
    std::cout << __func__ << std::endl;
  #endif
  std::cout << collection[value] << std::endl;
  if (value < collection.size() - 1)
  {
    printVector(collection, value + 1);
  }
  std::cout << collection[collection.size() - 1] << std::endl;
}

int main ()
{
  std::vector<int> collection = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  printVector(collection, 0);
  return 0;
}
