#include <iostream>

void calculate (std::initializer_list<int> li)
{
  int result = 0;
  for (const int* i = std::begin(li); i != std::end(li);++i)
    result += *i;
  std::cout << result << std::endl;
}

int main ()
{
  std::initializer_list<int> theList = {1, 2, 3, 4, 5};
  calculate(theList);
  return 0;
}
