#include <iostream>

int fact (int value)
{
  int result = 1;
  while (value > 1)
    result *= value--;
  return result;
}

int main ()
{
  int result = fact(10);
  std::cout << result << std::endl;
  return 0;
}
