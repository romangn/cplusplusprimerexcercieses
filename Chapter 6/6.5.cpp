#include <iostream>

int absolute (int value)
{
  int result;
  int counter = 0;
  if (value < 0)
  {
    while (value != 0)
    {
      ++value;
      ++counter;
    }
    result = counter;
  }
  else if (value >= 0)
    result = value;
  return result;
}

int abs (int value)
{
  return value > 0 ? value : -value;
}

int main ()
{
  std::cout << abs(-100) << std::endl;
  return 0;
}
