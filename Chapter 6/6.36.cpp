#include <iostream>

int (*func (int value)) [4]
{
  static int arr [4];
  for (int i = 0; i < 4;++i)
    arr [i] = value;
  int (*arra) [4] = &arr;
  return arra;
}

void doStuff (int* oldArr)
{
  for (int* it = oldArr; it != oldArr + 4; ++it)
    *it = 1;
}

std::string (&stringStuff(std::string& word)) [4]
{
  static std::string words [4];
  for (auto it = words; it != std::end(words);++it)
    *it = word;
  std::string (&result) [4] = words;
  return result;
}

void doStuffWithReference (std::string* words)
{
  for (std::string* it = words; it != words + 4; ++it)
    std::cin >> *it;
}

int main ()
{
  int (*arra) [4] = func(10);
  for (auto it = *arra; it != std::end(*arra);++it)
    std::cout << *it << std::endl;
  int test [4] = {5, 5, 5, 5};
  doStuff(test);
  for (auto it = test; it != std::end(test);++it)
    std::cout << *it << std::endl;
  std::string word = "Doge";
  std::string (&result) [4] = stringStuff(word);
  for (auto it = result; it != std::end(result);++it)
    std::cout << *it << std::endl;
  std::string dictionary [4];
  doStuffWithReference(dictionary);
  for (std::string* it = dictionary; it != dictionary + 4; ++it)
    std::cout << "The word is " << *it << std::endl;
  return 0;
}
