#include <iostream>

void print (const int* a , const int* b, int c)
{
  std::cout << "The integer is " << c << std::endl;
  for (const int* i = a; i != b;++i)
    std::cout << *i << std::endl;
}

int main ()
{
  int i = 0, j[2] = {0, 1};
  print(std::begin(j), std::end(j) , i);
  return 0;
}
