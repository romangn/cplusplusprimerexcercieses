#include <iostream>

void swap (int &a, int &b)
{
  int temp = a;
  a = b;
  b = temp;
}

int main ()
{
  int a, b;
  std::cin >> a >> b;
  int &first = a;
  int &second = b;
  std::cout << "Th a is " << a << ". The b is " << b << std::endl;
  swap (a, b);
  std::cout << a << " and " << b << std::endl;
  return 0;
}
