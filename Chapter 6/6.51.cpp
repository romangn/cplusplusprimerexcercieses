#include <iostream>

void f ()
{
  std::cout << "This is an empty function" << std::endl;
}

void f (int i )
{
  std::cout << "This takes one argument" << std::endl;
}

void f (int i, int j)
{
  std::cout << "Two integers" << std::endl;
}

void f (double d, double def = 3.14)
{
  std::cout << "Two doubles, second predefined" << std::endl;
}

int main ()
{
  //f (2.56, 42);
  f (42);
  f (42, 0);
  f (2.56, 3.14);
  return 0;
}
