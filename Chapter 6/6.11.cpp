#include <iostream>

void reset (int &value)
{
  value = 0;
}

int main ()
{
  int value = 42;
  int &a = value;
  reset(a);
  std::cout << value << std::endl;
  return 0;
}
