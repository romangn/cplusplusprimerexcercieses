#include <iostream>

int odd[] = {1, 3, 5, 7, 9};
int even[] = {2, 4, 6, 8, 10};

decltype (odd) &refPointer (int value)
{
  return (value % 2 == 0) ? even : odd;
}

int main ()
{
  for (auto it: refPointer(2))
    std::cout << it << std::endl;
  return 0;
}
