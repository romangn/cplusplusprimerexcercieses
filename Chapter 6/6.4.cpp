#include <iostream>

int fact ()
{
  int i;
  int result = 1;
  std::cin >> i;
  while (i <= 1)
    std::cin >> i;
  while (i > 1)
    result *= i--;
  return result;
}

int main ()
{
  std::cout << fact() << std::endl;
  return 0;
}
