#include <iostream>

int temporary ()
{
  static int counter;
  return ++counter;
}

int main ()
{
  for (int i = 0;i < 10;++i)
    std::cout << temporary() << std::endl;
  return 0;
}
