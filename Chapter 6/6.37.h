//Using the alias
using arr = std::string [10];
arr& func (arr& words);
//using trail
auto func (arr& words) -> std::string (&) [10];
//using decltype
std::string words [10];
decltype(words) &arrPtr (arr& collection)
