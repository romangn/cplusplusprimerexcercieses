#include <iostream>
#include <string>

bool capital (const std::string& s)
{
  bool result;
  for (std::string::const_iterator it = s.begin(); it != s.end(); ++it)
    {
      if (isupper(*it))
      {
        result = true;
        break;
      }
    }
  return result;
}

void toLower (std::string& s)
{
  for (std::string::iterator it = s.begin(); it != s.end();++it)
    *it = tolower(*it);
}

int main ()
{
  std::string word;
  std::cin >> word;
  std::string& reference = word;
  std::cout << capital(reference) << std::endl;
  toLower(reference);
  std::cout << reference << std::endl;
  std::cout << word << std::endl;
  return 0;
}
