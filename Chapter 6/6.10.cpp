#include <iostream>

void swap (int *a, int *b)
{
  int temp = *b;
  *b = *a;
  *a = temp;
}

int main ()
{
  int a, b;
  std::cin >> a >> b;
  std::cout << "A is " << a << " and b is " << b << std::endl;
  int *first = &a;
  int *second = &b;
  swap(first, second);
  std::cout << "New a is " << a << " and new b is " << b << std::endl;
  return 0;
}
