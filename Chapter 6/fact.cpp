#include <iostream>
#include "chapter6.h"

int fact (int value)
{
  int result = 1;
  while (value > 1)
    result *= value--;
  return result;
}
