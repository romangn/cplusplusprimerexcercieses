// 11.3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <map>
#include <algorithm>
#include <iterator>
#include <functional>

int main()
{
	std::map<std::string, size_t> words;
	std::string word;
	while (std::cin >> word) 
	{
		std::transform(word.begin(), word.end(), word.begin(), [](unsigned char c) {return tolower(c); });
		word.erase(std::remove_if(word.begin(), word.end(), ispunct), word.end());
		++words[word];
	}
	for (const auto it : words) { std::cout << it.first << " occurs " << it.second << ((it.second > 1)? " times." : " time.") << std::endl; }
    return 0;
}

