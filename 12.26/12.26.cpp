// 12.26.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <memory>


int main()
{
	int length = 50;
	std::allocator<std::string> alloc;
	std::string s;
	auto const p = alloc.allocate(length);
	auto q = p;
	while (std::cin >> s)
	{
		alloc.construct(q++, s);
	}
	const size_t size = q - p;
	for (int i = 0; i < size; i++)
	{
		std::cout << *(p + i) << std::endl;
	}
	while (q != p)
	{
		alloc.destroy(--q);
	}
	alloc.deallocate(p, 50);
    return 0;
}

