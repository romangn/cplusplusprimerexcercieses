// 12.14-12.15.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <memory>
#include <string>

struct destination
{
	size_t userIp = 0;
	std::string userName = "";
};

struct connection
{
	bool success = false;
	std::string message;
	connection connect(destination*);
};

void disconnect(connection);
void endConnection(connection* p);
void doStuff(destination& d);

int main()
{
	destination dest;
	doStuff(dest);
    return 0;
}

connection connection::connect(destination* d)
{
	std::cout << "Connected" << std::endl;
	return *this;
}

void disconnect(connection)
{
	std::cout << "Disconnected" << std::endl;
}

void endConnection(connection* p)
{
	disconnect(*p);
}

void doStuff(destination& d)
{
	connection c = c.connect(&d);
	std::shared_ptr<connection>p(&c, endConnection);
	// 12.15
	std::shared_ptr<connection>q(&c, [](connection* p) {disconnect(*p); });
}
