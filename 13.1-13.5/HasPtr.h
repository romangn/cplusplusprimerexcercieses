#pragma once
#include <iostream>
#include <string>

class HasPtr
{
public:
	HasPtr(const std::string &s = std::string()) :
		ps(new std::string(s)), i(0) { }
	HasPtr(const HasPtr& );
	std::string getValue() { return *ps; };
private:
	std::string* ps;
	int i;
};