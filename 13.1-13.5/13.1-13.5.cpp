// 13.1-13.5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "StrBlob.h"
#include "StrBlobPtr.h"
#include <iostream>
#include "HasPtr.h"

int main()
{
	// 13.1 Copy constructor is a type of constructor that is responsible for copying data (all members) from
	// one oject to the other. It is called when the new object is created from existing object
	// ie Obj ojb1 = obj2 or Obj obj1(obj2) or during creation of any temporary non reference values of this object type
	// such as passing object as an argument or returning object from function.
	// Must have a first parameter of reference to the class type.
	// Each member of the first object is assigned the value of the same member in the second object.
	// It is used with direct () and copy initializations = . The copy constructor will
	// copy data from obj2 assigning it to obj1 member by member.
	// Note that when both object are already created and exist the = operator will use not copy constructor
	// but assignment.

	// 13.2 Sales_data::Sales_data(Sales_data rhs); It is illegal because it takes a copy rather than reference. 
	// As the argument is passed by value, it will be attempted to make a copy of it by calling copy constructor
	// which will in turn call copy constructor again and so on until the program crashes.

	// 13.3 StrBlob each member of StrBlob gets properly copied
	StrBlob b1{"This", "is", "a", "test"};
	std::cout << b1.back() << std::endl;
	StrBlob b2 = b1;
	std::cout << b2.back() << std::endl;
	// StrBlobPtr also properly copies members
	StrBlob s1{ "Test" };
	StrBlobPtr sbPtr1(s1);
	std::cout << sbPtr1.deref() << std::endl;
	StrBlobPtr sbPtr2(sbPtr1);
	std::cout << sbPtr2.deref() << std::endl;

	// 13.4 1) When the arg is passed to the function a temporary constructor is used to create a copy
	// 2) Point local = arg When a local variable is created from arg
	// 3) *heap = new Point(global) When the pointer to heap dynamically allocates new Point from global
	// 4) Point pa[ 4 ] = { local, *heap } copying from local and value pointed by heap
	// 5) return *heap creation of temporary point from dereferencing *heap

	// 13.5
	HasPtr firstHasPtr("Test for the HasPtr");
	std::cout << firstHasPtr.getValue() << std::endl;
	HasPtr secondHasPtr(firstHasPtr);
	std::cout << secondHasPtr.getValue() << std::endl;
    return 0;
}

