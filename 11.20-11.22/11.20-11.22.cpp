// 11.20-11.22.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <map>

int main()
{
	// 11.20
	std::map<std::string, size_t> wordCount;
	std::string word;
	while (std::cin >> word)
	{
		auto ret = wordCount.insert({ word, 1 });
		if (!ret.second)
		{
			++ret.first->second;
		}
	}
	for (const auto& w : wordCount)
	{
		std::cout << w.first << " occurs " << w.second << ((w.second > 1) ? " times." : " time.") << std::endl;
	}
	// The original program that uses subscription is easier to read and write since it has no pointer operations

	// 11.21 
	// The loop increments the size_t counter. It does it by accessing the 
	// first parameter in a pair returned by insert operation (which is a pointer to the insered element)
	// then using this pointer it accesses the second element in a pair which is size_t

	// 11.22
	// std::pair<map<std::string, std::vector::int>, bool>::iterator it = coll.insert({"Test", {1, 2, 3, 4, 5}});
    return 0;
}

