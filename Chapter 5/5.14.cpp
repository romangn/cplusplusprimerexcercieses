#include <iostream>
#include <vector>

int main ()
{
  std::string sentence;
  unsigned int counter;
  std::string word;
  std::string tempword;
  getline(std::cin, sentence);
  for (std::string::iterator it = sentence.begin();it != sentence.end();++it)
  {
    if (*it != ' ')
      word += *it;
    else if (*it == ' ')
    {
      tempword = word;
      word = "";
    }
    if (word == tempword)
      ++counter;
  }
  if (counter > 0)
  std::cout << "The word is " << tempword << ". The counter is "<< counter <<  std::endl;
  else
    std::cout << "No duplicates found" << std::endl;
  return 0;
}
