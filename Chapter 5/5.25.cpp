#include <iostream>

int main ()
{
  int i1, i2;
  while (std::cin >> i1 >> i2)
  {
  try {
    if (i2 == 0)
      throw std::runtime_error ("Cannot divide by zero");
    else
      std::cout << (i1/i2) << std::endl;
  }
  catch (std::runtime_error err)
  {
    std::cout << err.what() << std::endl;
    std::cin >> i1 >> i2;
  }
}
  return 0;
}
