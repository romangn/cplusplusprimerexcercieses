#include <iostream>
#include <string>

int main ()
{
  std::string word1;
  std::string word2;
  do {
    std::cout << "Please enter the strings: " << std::endl;
    std::cin >> word1 >> word2;
    if (word1.length() > word2.length())
      std::cout << word1 << " is longer." << std::endl;
    else if (word2.length() > word1.length())
      std::cout << word2 << " is longer," << std::endl;
    else if (word1.length() == word2.length())
      std::cout << "Same length" << std::endl;
  }
  while(std::cin);
  return 0;
}
