#include <iostream>
#include <string>

int main ()
{
  std::string word1, word2;
  while (std::cin >> word1)
  {
    if (word1 == word2)
      break;
    word2 = word1;
  }
  if (word1 == word2)
    std::cout << word1 << std::endl;
  if (word1 != word2)
    std::cout << "No occurrances" << std::endl;
  return 0;
}
