#include <iostream>

using std::cout; using std::cin; using std::endl;

int main ()
{
  int i, j;
  cin >> i >> j;
  while (i <= j)
    ++i, cout << i;
  return 0;
}
