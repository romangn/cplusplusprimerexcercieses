#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> full = {1, 2, 3, 4, 5};
  std::vector<int> prefix = {1, 2, 3};
  bool result;
  int counter;
  if (full.size() > prefix.size())
  {
    for (auto it = std::begin(prefix); it != std::end(prefix);++it)
      {
      if (*it == full[counter])
      {
        result = true;
        ++counter;
      }
      else
      {
        result = false;
        break;
      }
      }
      if (result)
        std::cout << "Prefix" << std::endl;
      else
        std::cout << "Not prefix" << std::endl;
  }
  else if (full.size() == prefix.size())
  {
    for (auto it = std::begin(prefix);it != std::end(prefix);++it)
      {
        if (*it == full[counter])
        {
          result = true;
          ++counter;
        }
        else {
          result = false;
          break;
        }
      }
      if (result)
        std::cout << "The same" << std::endl;
      else
        std::cout << "Different" << std::endl;
  }
  return 0;
}
