#include <iostream>
using std::cin; using std::cout; using std::endl;

int main ()
{
  int i = 50;
  int j = 50;
  while (i <= 100)
    ++i, j += i, cout << j << endl;
  return 0;
}
