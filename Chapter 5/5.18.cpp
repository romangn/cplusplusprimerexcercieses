#include <iostream>

int main ()
{
  do
  {
    int v1, v2;
    std::cout << "Please enter 2 numbers" << std::endl;
    if (std::cin >> v1 >> v2)
      std::cout << "The sum is " << v1 + v2 << std::endl;
  }
  while (std::cin);
  return 0;
}
