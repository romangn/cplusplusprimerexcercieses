#include <iostream>
#include <vector>

using std::string; using std::cin; using std::cout; using std::endl;
using std::vector;

int main ()
{
  vector <string> grades = {"F", "D", "C", "B", "A", "A+"};
  int grade;
  cin >> grade;
  while (grade > 100 || grade < 0)
    cin >> grade;
  string result;
  (grade < 60) ? result = grades[0] : result = grades[(grade - 50) / 10];
  result += (grade == 100 || grade < 60)? "" : (grade % 10 > 7)? "+" : (grade % 10 < 3)? "-" : "";
  cout << result << endl;
  return 0;
}
