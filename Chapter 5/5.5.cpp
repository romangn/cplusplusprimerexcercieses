#include <iostream>
#include <vector>

using std::vector; using std::cin; using std::cout; using std::endl;
using std::string;

int main ()
{
  vector<string> grades = {"F", "D", "C", "B", "A", "A+"};
  int grade;
  string result;
  cin >> grade;
  while (grade > 100 || grade < 0)
    cin >> grade;
  if (grade < 60)
    result = grades[0];
  else
  {
    result = grades[(grade - 50) / 10];
    if (grade % 10 > 7)
      result += "+";
    else if (grade % 10 < 3)
      result += "-";
  }
  cout << result << endl;
  return 0;
}
