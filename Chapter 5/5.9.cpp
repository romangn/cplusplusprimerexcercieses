#include <iostream>
#include <string>

int main ()
{
  std::string word;
  getline(std::cin, word);
  int acount, icount, ucount, ecount, spacecount, tabcount, linecount, ffcount, flcount, ficount;
  for (std::string::iterator it = word.begin();it != word.end();++it)
  {
    *it = std::tolower(*it);
    if (*it == 'a')
      ++acount;
    else if (*it == 'i')
      ++icount;
    else if (*it == 'u')
      ++ucount;
    else if (*it == 'e')
      ++ecount;
    else if (*it == ' ')
      ++spacecount;
    else if (*it == '\t')
      ++tabcount;
    else if (*it == '\n')
      ++linecount;
    else if (*it == 'f' && *(it + 1) == 'f')
      ++ffcount;
    else if (*it == 'f' && *(it + 1) == 'l')
      ++flcount;
    else if (*it == 'f' && *(it + 1) == 'i')
      ++ficount;
  }
  std::cout << "The acount is: " << acount << ". The icount is " <<
    icount << ". The ucount is " << ucount << ". The ecount is " << ecount <<
    ". The spacecount is " << spacecount << ". The tabcount is " << tabcount <<
    ". The linecount is " << linecount << ". The ffcount is " <<
    ffcount << ". The flcount is " << flcount << ". The ficount is " <<
    ficount << std::endl;
  return 0;
}
