// 11.12.-11.13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>


int main()
{
	// 11.12
	std::vector<std::string> strings = { "First", "Second", "Third", "Fourth", "Fifth" };
	std::vector<size_t> numbers = { 1, 2, 3, 4, 5 };
	if (numbers.size() < strings.size()) { return -1; }
	std::vector<std::pair<std::string, size_t>> result;
	for (size_t i = 0; i < strings.size(); i++)
	{
		result.push_back({ strings[i], numbers[i] });
	}
	for (auto i : result) { std::cout << i.first << " " << i.second << "." << std::endl; }
	// 11.13
	std::pair<std::string, int> firstMethod;
	std::pair<std::string, int> test = std::make_pair("", 1);
	std::pair<std::string, int> thirdMethod("", 1);
	std::pair<std::string, int> foorthMethod = { "", 1 };
    return 0;
}

