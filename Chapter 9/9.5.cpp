#include <iostream>
#include <vector>
#include <stdexcept>

std::vector<int>::const_iterator contains (std::vector<int>::const_iterator it1, std::vector<int>::const_iterator it2, int value)
{
  bool result = false;
  while (it1 != it2)
  {
    if (it1 == it2)
    {
      throw std::logic_error ("No value found!");
    }
    if (*it1 == value)
    {
      break;
    }
    ++it1;
  }
  return it1;
}

int main ()
{
  std::vector<int> collection {1, 2, 3, 4, 5, 6, 7, 8, 9};
  std::vector<int>::const_iterator it1 = collection.cbegin();
  std::vector<int>::const_iterator it2 = collection.cend();
  std::cout << *contains (it1, it2, 9);
  return 0;
}
