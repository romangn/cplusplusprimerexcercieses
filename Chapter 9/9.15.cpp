#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> first {1, 2, 3, 4, 3};
  std::vector<int> second {1, 2, 3, 4, 5};
  if (first == second)
    std::cout << "Equal" << std::endl;
  return 0;
}
