#include <iostream>
#include <list>
#include <forward_list>

int main ()
{
  std::list<int> vi = {0, 1, 2, 3, 4 ,5 ,6 ,7, 8, 9, 10};
  auto iter = vi.begin();
  while (iter != vi.end())
  {
    if (*iter % 2)
    {
      iter = vi.insert(iter, *iter++);
      std::advance(iter, 2);
    }
    else
      iter = vi.erase(iter);
  }
  for (auto el : vi)
    std::cout << el << std::endl;
  return 0;
}
