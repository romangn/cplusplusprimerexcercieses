#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> v {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::vector<int>::iterator it = v.begin();
  while (it != v.end())
  {
    ++it;
    v.insert(it, 42);
    ++it;
  }
  for (auto el: v)
    std::cout << el << std::endl;
  return 0;
}
