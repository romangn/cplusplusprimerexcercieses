#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> coll;
  std::cout << coll.capacity() << std::endl;
  for (int i = 0; i < 100;++i)
  {
    coll.push_back(i);
    std::cout << "The element is " << i << std::endl;
    std::cout << coll.capacity() << std::endl;
  }
  return 0;
}
