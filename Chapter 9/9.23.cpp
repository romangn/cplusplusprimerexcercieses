#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> coll {1};
  if (!coll.empty())
  {
    std::cout << *coll.begin() << std::endl;
    std::cout << coll.front() << std::endl;
    std::cout << *(--coll.end()) << std::endl;
    std::cout << coll.back() << std::endl;
  }
  return 0;
}
