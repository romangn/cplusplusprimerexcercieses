#include <iostream>
#include <list>
#include <vector>

int main ()
{
  std::list<int> col {1, 2, 3, 4, 5, 6};
  std::vector<int> v1 {1, 2, 3, 4, 5, 6};
  std::vector<int> v2 {col.cbegin(), col.cend()};
  if (v1 == v2)
    std::cout << "Equal" << std::endl;
  return 0;
}
