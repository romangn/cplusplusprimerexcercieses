#include <iostream>

std::string &doStuff (std::string &s, const std::string &oldVal, const std::string &newVal)
{
  for (auto it = s.begin(); it != s.end();++it)
  {
    if (it == it + oldVal.size())
    {
      s.erase(it, it + oldVal.size());
      s.insert(it, newVal.begin(), newVal.end());
      it += newVal.size();
    }
  }
  return s;
}

int main ()
{
  std::string s = "thou";
  doStuff(s, "u", "rough");
  std::cout << s << std::endl;
  return 0;
}
