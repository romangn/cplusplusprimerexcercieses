#include <iostream>
#include <list>
#include <deque>

int main ()
{
  std::list<int> items {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::deque<int> evens;
  std::deque<int> odds;
  for (auto el : items)
  {
    if (el % 2 == 0)
      evens.push_back(el);
    else
      odds.push_back(el);
  }
  for (auto el : evens)
  {
    std::cout << "The evens are :" << std::endl;
    std::cout << el << std::endl;
  }
  for (auto el : odds)
  {
    std::cout << "The odds are: " << std::endl;
    std::cout << el << std::endl;
  }
  return 0;
}
