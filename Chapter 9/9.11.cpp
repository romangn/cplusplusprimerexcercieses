#include <iostream>
#include <vector>


int main ()
{
  std::vector<std::string> col1 (10);
  std::vector<std::string> col2 (10, "test");
  std::vector<std::string> col3 {"John", "Jack"};
  std::vector<std::string> col4;
  std::vector<std::string> col5 (col2);
  for (auto &el: col5)
    std::cout << el << std::endl;
  std::vector<std::string> col6 = col1;
  return 0;
}
