#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> col {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::vector<int>::iterator iter = col.begin();
  while (iter != col.end())
  {
    if (*iter % 2)
    {
      iter = col.insert(iter, *iter);
      ++iter;
    }
    ++iter;
  }
  for (auto el: col)
    std::cout << el << std::endl;
  return 0;
}
