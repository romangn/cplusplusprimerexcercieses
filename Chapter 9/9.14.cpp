#include <iostream>
#include <vector>
#include <list>

int main ()
{
  std::vector<std::string> strings;
  std::list<const char*> cstrings {"A test", "Dog"};
  strings.assign(cstrings.cbegin(), cstrings.cend());
  for (std::string el: strings)
    std::cout << el << std::endl;
  return 0;
}
