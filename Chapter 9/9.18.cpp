#include <iostream>
#include <deque>

int main ()
{
  std::string word;
  std::deque<std::string> coll;
  while (std::cin >> word)
  {
    coll.push_back(word);
  }
  std::deque<std::string>::const_iterator it = coll.begin();
  std::deque<std::string>::const_iterator jt = coll.end();
  while (it != jt)
  {
    std::cout << *it << std::endl;
    ++it;
  }
  return 0;
}
