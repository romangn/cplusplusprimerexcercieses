#include <iostream>
#include <vector>

int main ()
{
  std::vector<char> v{ 'h', 'e', 'l', 'l', 'o'};
  std::string s1 (v.begin(), v.end());
  std::cout << s1 << std::endl;
  return 0;
}
