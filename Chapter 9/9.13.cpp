#include <iostream>
#include <vector>
#include <list>

int main ()
{
  std::list<int> col1 (10, 5);
  std::vector<int> col2 (100, 9);
  std::vector<double> col3 (col1.begin(), col1.end());
  for (auto &el: col3)
    std::cout << el << std::endl;
  std::vector<double> col4 (col2.begin(), col2.end());
  for (auto el: col4)
    std::cout << el;
  return 0;
}
