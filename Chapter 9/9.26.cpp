#include <iostream>
#include <vector>
#include <list>

int main()
{
  int ia [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::vector<int> v (std::begin(ia), std::end(ia));
  std::list<int> l (std::begin(ia), std::end(ia));
  std::list<int>::iterator it = l.begin();
  while (it != l.end())
  {
    if (*it % 2 == 0)
      l.erase(it);
    ++it;
  }
  std::vector<int>::iterator jt = v.begin();
  while (jt != v.end())
  {
    if (*jt %2 != 0)
      v.erase(jt);
    ++jt;
  }
  for (auto el: l)
    std::cout << el << std::endl;
  std::cout << "Now for the evens!" << std::endl;
  for (auto el: v)
    std::cout << el << std::endl;
  return 0;
}
