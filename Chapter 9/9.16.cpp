#include <iostream>
#include <vector>
#include <list>

int main ()
{
  std::list<int> first {1, 2, 3, 4, 5, 6, 8};
  std::vector<int> second {1, 2, 3, 4, 5, 6};
  std::vector<int>::const_iterator b = second.begin();
  if (first.size() == second.size())
  {
    for (auto it = first.begin();it != first.end();++it)
    {
      if (*it != *b)
      {
        std::cout << "Not equal" << std::endl;
        break;
      }
      ++b;
    }
  }
  if (first.size() != second.size())
    std::cout << "Not equal size" << std::endl;
  return 0;
}
