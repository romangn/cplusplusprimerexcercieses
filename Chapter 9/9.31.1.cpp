#include <iostream>
#include <forward_list>

int main ()
{
  std::forward_list<int> vi = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  auto beforeIter = vi.before_begin();
  auto iter = vi.begin();
  while (iter != vi.end())
  {
    if (*iter % 2)
    {
      iter = vi.insert_after(beforeIter, *iter);
      std::advance(iter, 2);
      std::advance(beforeIter, 2);
    }
    else
      iter = vi.erase_after(beforeIter);
  }
  for (auto el : vi)
    std::cout << el << std::endl;
  return 0;
}
