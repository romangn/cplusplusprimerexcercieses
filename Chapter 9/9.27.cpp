#include <iostream>
#include <forward_list>

int main ()
{
  std::forward_list<int> l {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  std::forward_list<int>::iterator it = l.before_begin();
  std::forward_list<int>::iterator jt = l.begin();
  while (jt != l.end())
  {
    if (*jt % 2)
      l.erase_after(it);
    it = jt;
    ++jt;
  }
  for (auto el: l)
    std::cout << el << std::endl;
  return 0;
}
