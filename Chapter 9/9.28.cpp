#include <iostream>
#include <forward_list>

void processStrings (std::forward_list<std::string> &coll, const std::string &s1,
  const std::string &s2)
{
  std::forward_list<std::string>::iterator it = coll.begin();
  std::forward_list<std::string>::iterator jt = coll.before_begin();
  while (it != coll.end())
  {
    if (*it == s1)
    {
      coll.insert_after(it, s2);
      break;
    }
    jt = it;
    ++it;
    if (it == coll.end())
      coll.insert_after(jt, s2);
  }
}

int main ()
{
  std::forward_list<std::string> coll {"Dog", "Cat", "Bird", "Seal"};
  processStrings(coll, "Dog", "Parrot");
  for (auto el: coll)
    std::cout << el << std::endl;
  return 0;
}
