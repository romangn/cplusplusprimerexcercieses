#include <iostream>
#include <vector>

int main ()
{
  std::vector<int> coll (5, 10);
  std::cout << coll.at(0);
  std::cout << coll.front() << std::endl;
  std::cout << *coll.begin() << std::endl;
  return 0;
}
