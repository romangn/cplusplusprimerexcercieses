#include <iostream>
#include <list>

int main ()
{
  std::string word;
  std::list<std::string> coll;
  while (std::cin >> word)
  {
    coll.insert(coll.cend(),word);
  }
  std::list<std::string>::const_iterator it = coll.begin();
  std::list<std::string>::const_iterator jt = coll.end();
  while (it != jt)
  {
    std::cout << *it << std::endl;
    ++it;
  }
  return 0;
}
