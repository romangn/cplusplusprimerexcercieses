// 11.23.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <map>
#include <vector>

int main()
{
	std::multimap<std::string, std::vector <std::pair<std::string, size_t>> > families;
	std::string familyName, firstName, reply;
	size_t yob;
	std::cout << "Please enter the family name" << std::endl;
	std::cout << "Please press 1 if you want to add a family or 2 if you want to add members to existing family" << std::endl;
	std::cin >> reply;
	if (reply == "1")
	{
		std::cout << "Please enter the family name" << std::endl;
		std::cin >> familyName;
		families.insert({ familyName, {} });
	}
	else if (reply == "2")
	{
		std::cout << "Please enter the family name" << std::endl;
		std::cin >> familyName;
		auto rememberedFamily = families.insert({ familyName, {} });
		std::cout << "Now Please enter the members first name followed by date of birthday" << std::endl;
		while (std::cin >> firstName >> yob)
		{
			rememberedFamily->second.push_back({ firstName, yob });
		}
	}
	for (auto it : families)
	{
		std::cout << "The family name is " << it.first << std::endl;
		size_t size = it.second.size();
		if (size == 0) { std::cout << "There are no members" << std::endl; }
		else
		{
			std::cout << "The family members are: " << std::endl;
			for (auto member : it.second) { std::cout << member.first << " " << member.second << ", "; }
			std::cout << std::endl;
		}
	}
	return 0;
}

