#include <iostream>
#include <string>

using std::string; using std::cin; using std::cout;
using std::endl;

int main ()
{
    string s1, s2;
    getline(cin, s1);
    getline(cin, s2);
    if (s1 == s2)
        cout << "Equal content" << endl;
    else
        {
            if(s1.size() == s2.size())
                cout << "Same length" << endl;
            else
                if(s1.size() > s2.size())
                    cout << s1 << endl;
            else
                if(s2.size() > s1.size())
                    cout << s2 << endl;
        }
    return 0;
}
