#include <iostream>

using std::cout;using std::endl;
using std::end; using std::begin;

int main ()
{
    int v1 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int v2 [] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int *first_begin = v1;
    int *second_begin = v2;
    auto n1 = end(v1) - begin(v1);
    auto n2 = end(v2) - begin(v2);
    if (n1 == n2)
    {
        while(first_begin != end(v1))
        {
            if(*first_begin == *second_begin)
            {
                ++first_begin;
                ++second_begin;
            }
            else
            {
                cout << "Different" << endl;
                return - 1;
            }
        }
    }
    else
    {
        cout << "Different length" << endl;
        return - 1;
    }
    cout << "The arrays are the same!" << endl;
    return 0;
}
