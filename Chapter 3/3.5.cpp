#include <iostream>
#include <string>

using std::cin; using std::cout; using std::endl;
using std::string;

int main ()
{
    string s1, s2;
    while(cin >> s1)
    {
        s2 += s1 + " ";
        if(s2.size() >= 10)
        {
            cout << s2 << endl;
            break;
        }
    }
    return 0;
}
