#include <iostream>
#include <vector>

using std::cout; using std::endl; using std::cin;
using std::vector;

int main ()
{
    vector <unsigned> scores (11, 0);
    unsigned grade;
    while (cin >> grade)
    {
        if (grade <= 100)
        {
            auto counter = grade/10;
            *(scores.begin() + counter) += 1;
        }
        if (grade > 100)
            break;
    }
    for (auto it = scores.cbegin();it != scores.cend();++it)
        cout << *it << endl;
    return 0;
}
