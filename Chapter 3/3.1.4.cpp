#include <iostream>
#include <string>

using std::cin; using std::cout; using std::endl;
using std::string;

struct Sales_data {
    string bookNo;
    int quantity = 0;
    double revenue = 0.0;
};
int main()
{
    Sales_data item1, item2;
    cin >> item1.bookNo >> item1.quantity >> item1.revenue;
    cin >> item2.bookNo >> item2.quantity >> item2.revenue;
    if(item1.bookNo == item2.bookNo)
        cout << (item1.quantity * item1.revenue) +
            (item2.quantity * item2.revenue);
    else
        cout << "Not the same item!" << endl;
    return 0;
}
