#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;

int main ()
{
    vector <int> v1 (10, 42);
    vector <int> v2 {42, 42, 42, 42, 42,
        42, 42, 42, 42, 42};
    vector <int> v3 (10);
    for (auto &i : v3)
        i = 42;
    for (auto i: v3)
        cout << i << endl;
    return 0;
}
