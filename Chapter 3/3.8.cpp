#include <iostream>
#include <string>

using std::string;
using std::cout; using std::endl;

int main ()
{
    string s1("This is a test");
    int counter = 0;
    while(counter < s1.size())
    {
        if(!isspace(s1[counter]))
        {
        s1[counter] = 'X';
        }
        counter++;
    }
    cout << s1 << endl;
    return 0;
}
