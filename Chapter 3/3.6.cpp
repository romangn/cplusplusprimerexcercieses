#include <iostream>
#include <string>

using std::cout; using std::endl;
using std::string;

int main ()
{
    string s1 = "This is a test";
    for (auto &c : s1)
    {
        if(!isspace(c))
            c = 'X';
    }
    cout << s1 << endl;
    return 0;
}
