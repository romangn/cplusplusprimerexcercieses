#include <iostream>

using std::cout; using std::endl;

int main ()
{
    constexpr size_t array_size = 10;
    int ia [array_size];
    int ia2 [array_size];
    for (size_t i = 1;i <= array_size;++i)
        ia[i - 1] = i;
    for (size_t i = 0; i <= array_size - 1;++i)
        ia2[i] = ia[i];
    for (auto i: ia)
        cout << i << endl;
    for (auto i: ia2)
        cout << i << endl;
    return 0;
}
