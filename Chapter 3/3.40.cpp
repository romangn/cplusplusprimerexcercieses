#include <iostream>
#include <cstring>

using std::cout; using std::endl;

int main ()
{
    const char s1 [] = "Hello";
    const char s2 [] = "World";
    char s3 [(strlen(s1) + strlen(s2)) + 1];
    strcpy(s3, s1);
    strcat(s3, " ");
    strcat(s3, s2);
    const char *p = s3;
    while (*p)
    {
        cout << *p;
        ++p;
    }
    return 0;
}
