#include <iostream>

using std::cout; using std::endl; using std::cin;

int main()
{
    int num1, num2;
    cin >> num1 >> num2;
    while(num1 <= num2)
    {
        cout << num1 << endl;
        num1++;
    }
    return 0;
}
