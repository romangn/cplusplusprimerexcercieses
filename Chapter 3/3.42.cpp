#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;

int main ()
{
    vector <int> the_vector = {1, 2, 3, 4, 5, 6};
    int array [the_vector.size()];
    int *p = array;
    for (auto it = the_vector.begin();it != the_vector.end();++it)
    {
        *p = *it;
        ++p;
    }
    for (auto i: array)
        cout << i << endl;
    return 0;
}
