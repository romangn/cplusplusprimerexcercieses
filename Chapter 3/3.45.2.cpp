#include <iostream>

using std::cout; using std::endl;
using std::end;

int main ()
{
    int ia [3][4] =
    {
        {1, 2, 3, 4},
        {5, 6, 7, 8},
        {9, 10, 11, 12}
    };
    for (auto row = ia; row != end(ia);++row)
        for (auto col = *row; col != end(*row);++col)
            cout << *col << endl;
    return 0;
}
