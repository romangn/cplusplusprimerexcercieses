#include <iostream>
#include <string>

using std::cout; using std::cin; using std::endl;
using std::string;

int main ()
{
    string s1, s2;
    getline(cin, s1);
    for (auto &c : s1)
    {
        if (!ispunct(c))
            s2 += c;
    }
    cout << s2 << endl;
    return 0;
}
