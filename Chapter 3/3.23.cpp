#include <iostream>
#include <vector>

using std::cout;using std::endl;
using std::vector;

int main ()
{
    vector <int> collection {1, 2, 3, 4, 5, 6, 7,
        8, 9, 10};
    for (auto it = collection.begin();it != collection.end();++it)
        *it *= 2;
    for (auto it: collection)
        cout << it << endl;
    return 0;
}
