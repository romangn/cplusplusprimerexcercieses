#include <iostream>
#include <vector>
#include <string>
#include <cctype>

using std::cout;using std::endl; using std::cin;
using std::vector; using std::string;

int main ()
{
    string t;
    vector <string> v1;
    while(cin >> t)
    {
        if (t == "stop")
            break;
        v1.push_back(t);
    }
    for (auto it = v1.begin();it != v1.end() && !it -> empty();++it)
    {
        for (auto &i: *it)
            i = toupper(i);
    }
    for (auto it = v1.cbegin();it != v1.cend();++it)
        cout << *it << endl;
    return 0;
}
