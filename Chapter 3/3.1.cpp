#include <iostream>

using std::cout; using std::endl;

int main()
{
    int i = 50;
    int result;
    while(i <= 100)
    {
        i++;
        result += i;
    }
    cout << result << endl;
    return 0;
}
