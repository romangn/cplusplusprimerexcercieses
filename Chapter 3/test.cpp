#include <iostream>
#include <cstring>

using std::cout;

int main ()
{
    const char s1 [] = {'H', 'e', 'l', 'l', 'o', '\0'};
    const char s2 [] = {'W', 'o', 'r', 'l', 'd', '\0'};
    const char *p = s1;
    const char *p2 = s2;
    while (*p)
    {
        cout << *p;
        ++p;
    }
    while (*p2)
    {
        cout << *p2;
        ++p2;
    }
    return 0;
}
