#include <iostream>
#include <string>

using std::string; using std::cout; using std::endl;

int main ()
{
    string s1("This is a test");
    for(int counter = 0;counter < s1.size();counter++)
    {
        if(!isspace(s1[counter]))
            s1[counter] = 'X';
    }
    cout << s1 << endl;
    return 0;
}
