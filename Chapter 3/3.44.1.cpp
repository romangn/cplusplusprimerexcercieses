#include <iostream>

using std::cout; using std::endl;
using std::begin; using std::end;
using int_array = int [4];

int main ()
{
    int ia [3][4] =
    {
        {1, 2, 3, 5},
        {5, 6, 7, 8},
        {9, 10, 11, 12}
    };
    for (int_array (&row): ia)
        for (int &col: row)
            cout << col << endl;
    return 0;
}
