#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;
using std::begin; using std::end;

int main ()
{
    int arr [] = {1, 2, 3, 4, 5, 6};
    vector <int> new_arr(begin(arr), end(arr));
    for (auto it = new_arr.begin();it != new_arr.end();++it)
        cout << *it << endl;
    return 0;
}
