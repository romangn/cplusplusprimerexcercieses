#include <iostream>

using std::cin; using std::cout; using std::endl;using std::string;

struct Sales_data {
    string bookNo;
    int quantity = 0;
    double revenue = 0.0;
};

int main()
{
    int counter;
    Sales_data item1, item2;
    if(cin >> item1.bookNo >> item1.quantity >> item1.revenue)
    {
        while(cin >> item2.bookNo >> item2.quantity >> item2.revenue)
        {
            if(item2.bookNo == item1.bookNo)
                counter++;
            else
                {
                    cout << counter;
                    counter = 0;
                    item1.bookNo = item2.bookNo;
                }
        }
    }
    return 0;
}
