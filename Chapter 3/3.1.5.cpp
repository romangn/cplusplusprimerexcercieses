#include <iostream>

using std::cout; using std::endl; using std::string; using std::cin;

struct Sales_data {
    string bookNo;
    int quantity = 0;
    double revenue = 0.0;
};

int main()
{
    Sales_data item1, item2;
    int total;
    if(cin >> item1.bookNo >> item1.quantity >> item1.revenue)
    {
        while(cin >>item2.bookNo >> item2.quantity >> item2.revenue)
        {
            if(item2.bookNo == item1.bookNo)
                total += item2.quantity * item2.revenue;
            else
                break;
        }
    }
    cout << "The total is: " << total << " " << endl;
    return 0;
}
