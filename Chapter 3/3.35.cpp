#include <iostream>
#include <iterator>

using std::cout; using std::endl;
using std::end;

int main ()
{
    int v1 [] = {1, 2 , 3, 4, 5, 6, 7, 8, 9, 10};
    int *p = v1;
    int *last = end(v1);
    while(p != last)
    {
        *p = 0;
        ++p;
    }
    for (auto i: v1)
        cout << i << endl;
    return 0;
}
