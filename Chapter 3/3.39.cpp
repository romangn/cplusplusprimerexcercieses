#include <iostream>
#include <string>

using std::cin; using std::cout; using std::endl;
using std::string;

int main ()
{
    string s1, s2;
    cin >> s1;
    cin >> s2;
    auto n = s1.size();
    auto n2 = s2.size();
    if (n == n2)
    {
        auto i = s2.begin();
        for (auto it = s1.begin();it != s1.end();++it)
        {
            if(*it == *i)
                ++i;
            else
            {
                cout << "Different" << endl;
                return - 1;
            }
        }

    }
    else
    {
        cout << "Different length!" << endl;
        return - 1;
    }
    cout << "Same!" << endl;
    return 0;
}
