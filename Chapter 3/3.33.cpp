#include <iostream>

using std::cout; using std::endl; using std::cin;

int main ()
{
    unsigned scores [11] = {};
    unsigned grade;
    while (cin >> grade)
    {
        if (grade <= 100)
            ++scores[grade/10];
        else
            break;
    }
    for (auto i: scores)
        cout << i << endl;
    return 0;
}
