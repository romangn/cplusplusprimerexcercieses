#include <iostream>

using std::cin; using std::cout; using std::endl; using std::string; using std::cerr;

struct Sales_data {
    string bookNo;
    int quantity = 0;
    double revenue = 0.0;
};

int main ()
{
    Sales_data total;
    int result;
    if(cin >> total.bookNo >> total.quantity >> total.revenue)
    {
        Sales_data trans;
        while(cin >> trans.bookNo >> trans.quantity >> trans.revenue)
        {
            if(total.bookNo == trans.bookNo)
                result += (total.quantity * total.revenue) *
                    (trans.quantity * trans.revenue);
            else
            {
                cout << result << endl;
                total.bookNo = trans.bookNo;
            }
        }
        cout << result << endl;
    }
    else
    {
        cerr << "No data!";
        return - 1;
    }
    return 0;
}
