#include <iostream>

using std::cout; using std::endl;
using std::begin; using std::end;

int main ()
{
    int ia [3][4] = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};
    for (int (&row) [4]: ia)
        for (int col: row)
            cout << col << endl;
    return 0;
}
