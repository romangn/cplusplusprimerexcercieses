#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;

int main ()
{
    vector <int> v1 {1, 2, 3, 4, 5, 6, 7, 8,
        9, 10};
    for (auto it = v1.cbegin();it + 1 != v1.cend();++it)
        cout << *it + *(it + 1) << endl;
    return 0;
}
