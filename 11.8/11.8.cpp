// 11.8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

int main()
{
	std::map<std::string, size_t> wordCount;
	// The advantage of set here would be that it would use its internal version of find rather than using the STL
	// algo. In addition to this set will allow for a completely unique elements
	std::vector<std::string> exclusions = { "the", "but", "and", "or", "an", "a",
		"The", "But", "And", "Or", "An",
		"A" };
	std::string word;
	while (std::cin >> word)
	{
		if (std::find(exclusions.begin(), exclusions.end(), word) == exclusions.end())
		{
			++wordCount[word];
		}
	}
	for (auto i : wordCount)
	{
		std::cout << "The word " << i.first << " occurs " << i.second<< ((i.second > 1)? " times." : " time.")<< std::endl;
	}
    return 0;
}

