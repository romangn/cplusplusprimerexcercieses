// 13.14-13.17.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "numbered.h"



void f(const numbered& s);

int main()
{	// 13.14 Since it uses synthesized copy control, it would overwrite the unique serial number when the object is copied
	// Each function would display the same mysn, since its the same everywhere after being copied

	// 13.15 The copy constructor would generate different serial number for each object on creation.
	// However each function would display a different serial number on different run since it takes a copy as a parameter
	// and each copy is created with a different serial number. Though it wont be saved.

	// 13.16 Since the function does not take a copy, it would not generate a copy and would display the proper
	// value on each run. It does not call a copy constructor since its passed by reference

	// 13.17
	numbered a, b = a, c = b;
	f(a);
	f(b);
	f(c);
	f(a);
	f(b);
	f(c);
	f(a);
	f(b);
	f(c);

    return 0;
}

void f(const numbered& s)
{
	std::cout << s.mysn << std::endl;
}
