#include "stdafx.h"
#include "QueryResult.h"
#include "StrBlob.h"

using line_no = std::vector<std::string>::size_type;

std::ostream& print(std::ostream& os, const QueryResult& qr)
{
	os << qr.sought << " occurs " << qr.lines->size() << " "
		<< make_plural(qr.lines->size(), "time", "s") << std::endl;
	for (auto num : *qr.lines)
		os << "\t(line " << num + 1 << ")"
		<< *(qr.sharedPtrToFileStrBlob->getData() + num) << std::endl;
	return os;
}

std::string make_plural(size_t size, const std::string& word,
	const std::string& end)
{
	return size == 1 ? word : word + end;
}

std::set<line_no>::iterator QueryResult::begin()
{
	return lines->begin();
}

std::set<line_no>::iterator QueryResult::end()
{
	return lines->end();
}



std::shared_ptr<std::vector<std::string>> QueryResult::getFile()
{
	return file;
}