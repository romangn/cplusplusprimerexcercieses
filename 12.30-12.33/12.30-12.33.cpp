// 12.30-12.33.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TextQuery.h"
#include "QueryResult.h"
#include <iostream>

void runQueries(std::ifstream& infile);

int main()
{
	// 12.30
	std::ifstream file("input.txt");
	runQueries(file);
	// 12.31 Vector would allow duplicated therefor it would be able to show if the word also occurs multiple times on the same line
	// (set implementation allows only for 1 line)
    return 0;
}

void runQueries(std::ifstream& infile)
{
	TextQuery tq(infile);
	while (true)
	{
		std::cout << "enter the word to look for, or q to quit: ";
		std::string s;
		if (!(std::cin >> s) || s == "q") break;
		print(std::cout, tq.query(s)) << std::endl;;
	}
}


