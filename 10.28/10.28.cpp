// 10.28.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <list>


int main()
{
	std::vector<int> initial = { 1, 2, 3, 4, 5, 6 ,7 ,8, 9 };
	std::vector<int> vec1, vec3;
	std::list<int> list1;
	std::copy(initial.begin(), initial.end(), std::back_inserter(vec1));
	std::cout << "Back inserter to container 1" << std::endl;
	for (auto it : vec1) { std::cout << it << std::endl; }
	std::copy(initial.begin(), initial.end(), std::front_inserter(list1));
	std::cout << "Front inserter to container 2" << std::endl;
	for (auto it : list1) { std::cout << it << std::endl; }
	std::copy(initial.begin(), initial.end(), std::inserter(vec3, vec3.begin()));
	std::cout << "Inserter to container 3" << std::endl;
	for (auto it : vec3) { std::cout << it << std::endl; }

    return 0;
}

