// 10.33.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>


int main()
{
	std::ifstream in("input.txt");
	std::istream_iterator<int> intStream(in), eof;
	std::ofstream oddOut("odd.txt");
	std::ofstream evenOut("even.txt");
	std::ostream_iterator<int> outOddStream(oddOut, " ");
	std::ostream_iterator<int> outEvenStream(evenOut, "\n");
	while (intStream != eof)
	{
		if ((*intStream % 2) == 0)
		{
			*outEvenStream++ = *intStream++;
		}
		else
		{
			*outOddStream++ = *intStream++;
		}
	}
    return 0;
}

