// 11.31-11.32.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <set>

int main()
{
	// 11.31
	std::multimap<std::string, std::string> theMap;
	theMap.insert({ "Dostoyevsky", "War and peace" });
	theMap.insert({ "Akhmatova", "Test" });
	theMap.insert({ "Akhmatova", "Dest" });
	theMap.insert({ "Test author", "Test book" });
	auto itemToRemove = theMap.find("Test author");
	if (itemToRemove != theMap.end()) { theMap.erase(itemToRemove); }
	// 11.32
	// map and not multimap because multimap does not allow subscript
	std::map<std::string, std::multiset<std::string>> sortedMap;
	for (const auto& it : theMap)
	{
		sortedMap[it.first].insert(it.second);
	}
	for (const auto& autors: sortedMap)
	{
		std::cout << autors.first << ":" << std::endl;
		for (const auto& books : autors.second) { std::cout << books << std::endl; }
	}
    return 0;
}

