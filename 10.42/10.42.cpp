// 10.42.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <algorithm>
#include <list>

void elimDups(std::list<std::string>& words);

int main()
{
	std::list<std::string> Items = { "dog", "cat", "seal", "bird", "cat", "bird", "dog" };
	elimDups(Items);
    return 0;
}

void elimDups(std::list<std::string>& words)
{
	std::cout << "Original list" << std::endl;
	for (auto it : words) { std::cout << it << std::endl; }
	words.sort();
	std::cout << "Sorted list" << std::endl;
	for (auto it : words) { std::cout << it << std::endl; }
	words.unique();
	std::cout << "Duplicates removed" << std::endl;
	for (auto it : words) { std::cout << it << std::endl; }

}

