// 11.24-11.26.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <map>


int main()
{
	// 11.24 It creates a new element in the map and sets its value to 1

	// 11.25 It gives index out of bound since the vector is empty

	// 11.25
	std::map<int, std::string> test;
	test[1] = "Test";
	using KeyType = std::map<int, std::string>::key_type;

	std::cout << "type to subscript: " << typeid(KeyType).name() << std::endl;
	std::cout << "returned from the subscript operator: " << typeid(decltype(test[1])).name() << std::endl;


    return 0;
}

