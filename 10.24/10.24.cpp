// 10.24.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include <functional>
#include <string>

using namespace std::placeholders;

bool checkSize(size_t n, std::string& s);

int main()
{
	std::vector<int> values = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::string word;
	std::cin >> word;
	auto wc = std::find_if(values.begin(), values.end(), std::bind(checkSize, _1, word));
	std::cout << *wc << std::endl;
    return 0;
}

bool checkSize(size_t n, std::string& s)
{
	return n > s.length();
}

