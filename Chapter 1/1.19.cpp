#include <iostream>

int main()
{
    int v, v1;
    std::cin >> v >> v1;
    if(v > v1)
    {
        while(v1 <= v)
        {
            std::cout << v1 << std::endl;
            v1++;
        }
    }
    else
    {
        while(v <= v1)
        {
            std::cout << v << std::endl;
            v++;
        }
    }
    return 0;
}
