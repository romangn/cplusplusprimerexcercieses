#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item item1, item2;
    if(std::cin >> item2)
    {
        while(std::cin >> item1)
        {
            if(item1.isbn() == item2.isbn())
                item2 += item1;
            else
                break;
        }
    }
    std::cout << item2 << std::endl;
    return 0;
}
