#include <iostream>
#include "Sales_item.h"

int main()
{
    Sales_item item1, item2;
    int counter;
    if(std::cin >> item1)
    {
        while(std::cin >> item2)
        {
            if(item2.isbn() == item1.isbn())
                counter++;
            else
                break;
        }
        std::cout << counter << std::endl;
    }
    return 0;
}
