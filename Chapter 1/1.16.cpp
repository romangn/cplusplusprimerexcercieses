#include <iostream>

int main()
{
    int v , sum;
    while(std::cin >> v)
        sum += v;
    std::cout << sum << std::endl;
    return 0;
}
