#include <iostream>

int main()
{
    int v1, v2;
    if (std::cin >> v1)
    {
        int counter = 1;
        while(std::cin >> v2)
        {
            if(v2 == v1)
                counter++;
            else
            {
                std::cout << v1 << " occurs "
                          << counter << " times" << std::endl;
                v1 = v2;
                counter = 1;
            }
        }
        std::cout << v1 << " occurs "
                  << counter << " times" << std::endl;
    }
    return 0;
}
