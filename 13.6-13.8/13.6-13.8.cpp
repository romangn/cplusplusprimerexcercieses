// 13.6-13.8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "StrBlob.h"
#include "StrBlobPtr.h"
#include "HasPtr.h"
#include <iostream>


int main()
{
	// 13.6 Copy assignment operator is a class operator that is responsible for assigning values between
	// objects of the same class type. It overrides the default = operator for the particular class type.
	// It takes reference to the object and returns reference to the left hand operator Foo& operator=(const Foo&).
	// Like copy constructor it assignes each data member's values from the right hand object to members of the left
	// hand object and then returns reference left hand object. Array members are assigned
	// by assigning each element in the array. A synthesized copy assignment
	// operator is also created even if its not defined (unless its specifically deleted).
	// Note that it works on the objects that are already created ie Ojb o1 Ojb o2 o1 = o2
	// Creating new object ie Obj obj2 = obj1 will use copy constructor since the new object is being created

	// 13.7 StrBlob StrBlob assigns each member from the right hand side to the left hand side
	StrBlob b1{ "This", "is", "a", "test" };
	StrBlob b2{ "This", "is", "a", "trap" };
	std::cout << b1.back() << std::endl;
	std::cout << b2.back() << std::endl;
	b1 = b2;
	std::cout << b1.back() << std::endl;
	std::cout << b2.back() << std::endl;
	// 13.7 StrBlobPtr StrBlobPtr assigns each member from the right hand side to the left hand side
	StrBlob s1{ "Test" };
	StrBlobPtr sbPtr1(s1);
	std::cout << sbPtr1.deref() << std::endl;
	StrBlobPtr sbPtr2;
	sbPtr2 = sbPtr1;
	std::cout << sbPtr1.deref() << std::endl;
	std::cout << sbPtr2.deref() << std::endl;

	// 13.8
	HasPtr p1("HasPtr p1 Test");
	HasPtr p2("HasPtr p2 Test");
	std::cout << p1.getValue() << std::endl;
	std::cout << p2.getValue() << std::endl;
	p1 = p2;
	std::cout << p1.getValue() << std::endl;
	std::cout << p2.getValue() << std::endl;
}

