// 12.6-12.9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <memory>

std::vector<int>* createVector();
std::vector<int>* fillValues(std::vector<int>* vec);
void readVector(std::vector<int>* vec);

std::shared_ptr<std::vector<int>> createVectorShared();
void fillValuesShared(std::shared_ptr<std::vector<int>> vec);
void readVectorShared(std::shared_ptr<std::vector<int>> vec);

int main()
{
	// 12.6
	//std::vector<int>* vec = createVector();
	//vec = fillValues(vec);
	//readVector(vec);
	//vec = nullptr;

	// 12.7
	std::shared_ptr<std::vector<int>> testVec = createVectorShared();
	fillValuesShared(testVec);
	readVectorShared(testVec);

	// 12.8 The function supposed to return a boolean value as per signature yet it returns pointer to integer

	// 12.9 Firstly the two pointers to integers are created, the first one to 42 and the second to 100
	// After that the second pointer that used to point to 100 is assigned to point to the first pointer so now both of the
	// pointers point to 42. After that 2 shared pointers are created, one pointing to 42 and the second one to 100.
	// Finally the second shared pointer is assigned the value of the first pointer so now both of the shared pointers point to 42.
	// In the case of shared pointers value 100 is completely erased from memory as nothing points to it anymore. However in case
	// of normal pointers it still exists somewhere in memory as it was not deleted.
    return 0;
}

std::vector<int>* createVector()
{
	return new std::vector<int>();
}

std::vector<int>* fillValues(std::vector<int>* vec)
{
	int i = 0;
	while (std::cin >> i)
	{
		vec->push_back(i);
	}
	return vec;
}

void readVector(std::vector<int>* vec)
{
	std::cout << "Printing the pointer values" << std::endl;
	for (int i = 0; i <= vec->size(); ++i)
	{
		std::cout << vec->at(i) << std::endl;
	}
	delete vec;
}

std::shared_ptr<std::vector<int>> createVectorShared()
{
	return std::make_shared<std::vector<int>>();
}

void fillValuesShared(std::shared_ptr<std::vector<int>> vec)
{
	int i = 0;
	while (std::cin >> i)
	{
		vec->push_back(i);
	}
}

void readVectorShared(std::shared_ptr<std::vector<int>> vec)
{
	std::cout << "Printing the shared pointer values" << std::endl;
	for (auto i = 0; i <= vec->size(); ++i)
	{
		std::cout << vec->at(i) << std::endl;
	}
}
