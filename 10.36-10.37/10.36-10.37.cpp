// 10.36-10.37.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <list>
#include <vector>
#include <iostream>
#include <algorithm>


int main()
{
	std::list<int> items = { 1, 2, 3, 4, 0, 5, 0, 7, 0, 9, 1, 0 ,5 ,6, 7, 0, 8, 9 };
	// 10.36
	auto last = std::find(items.crbegin(), items.crend(), 0);
	std::cout << *last << std::endl;
	// 10. 37
	std::cout << "Now for the second exercies" << std::endl;
	std::vector<int> listOfItems = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	std::list<int> theList;
	std::copy(listOfItems.crbegin() + 2, listOfItems.crend() - 3, std::back_inserter(theList));
	for (auto it : theList) { std::cout << it << std::endl; }
    return 0;
}

