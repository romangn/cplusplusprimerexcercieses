// 11.33-11.36.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <stdexcept>


std::map<std::string, std::string> buildMap(std::ifstream& mapFile);
const std::string& transform(const std::string& s, const std::map<std::string, std::string>& map);
void transformWord(std::ifstream& mapFile, std::ifstream& inputFile);

int main()
{
	// 11.33
	std::ifstream rulesFile;
	std::ifstream inputFile;
	rulesFile.open("rules.txt");
	inputFile.open("input.txt");
	transformWord(rulesFile, inputFile);
	rulesFile.close();
	inputFile.close();
    return 0;

	// 11.34
	// If we use the subscriptor then the program would actually create a key with the value

	// 11.35
	// Same effect

	// 11.36
	// The file wont be read
}

std::map<std::string, std::string> buildMap(std::ifstream& mapFile)
{
	std::map<std::string, std::string> returnedMap;
	std::string key;
	std::string value;
	while (mapFile >> key && std::getline(mapFile, value))
	{
		if (value.size() > 1)
		{
			returnedMap[key] = value.substr(1);
		}
		else
			throw std::runtime_error("No value found for the key " + key);
	}
	return returnedMap;
}

const std::string& transform(const std::string& s, const std::map<std::string, std::string>& map)
{
	auto it = map.find(s);	
	if (it != map.cend())
	{
		return it->second;
	}
	else
		return s;
}

void transformWord(std::ifstream& mapFile, std::ifstream& inputFile)
{
	auto map = buildMap(mapFile);
	std::string text;
	while (std::getline(inputFile, text))
	{
		std::istringstream stream(text);
		std::string word;
		bool firstWord = true;
		while (stream >> word)
		{
			if (firstWord)
			{
				firstWord = false;
			}
			else
			{
				std::cout << " ";
			}
			std::cout << transform(word, map);
		}
		std::cout << std::endl;
	}
}