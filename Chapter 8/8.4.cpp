#include <iostream>
#include <fstream>
#include <vector>

int main ()
{
  std::vector<std::string> words;
  std::ifstream input("test.txt");
  std::string line;
  while (input)
  {
  std::string temp;
  getline(input, line);
  words.push_back(line);
  }
  for (auto it = words.begin(); it != words.end();++it)
    std::cout << *it << std::endl;
  return 0;
}
