#include <iostream>
#include <fstream>
#include <vector>

int main ()
{
  std::ifstream input("test.txt");
  std::vector<std::string> words;
  std::string line;
  while (input)
  {
    std::string word;
    getline(input, line);
    for (auto el: line)
    {
      word += el;
      if (el == ' ')
      {
        words.push_back(word);
        word = "";
      }
    }
    words.push_back(word);
  }
  for (auto it = words.begin(); it != words.end();++it)
    std::cout << *it << std::endl;
  return 0;
}
