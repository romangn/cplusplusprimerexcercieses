#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

int main ()
{
  std::vector<std::string> words;
  std::string line;
  std::ifstream input ("test.txt");
  while (getline(input, line))
  {
    words.push_back(line);
  }
  for (auto el: words)
  {
    std::istringstream theline (el);
    std::cout << theline.str() << std::endl;
  }
  return 0;
}
