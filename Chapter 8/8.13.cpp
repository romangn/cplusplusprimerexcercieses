#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

struct PersonInfo
{
  std::string name;
  std::vector<std::string> phones;
};

bool valid (const std::string &word)
{
  bool result = true;
  for (auto el : word)
  {
    if (!isdigit(el))
    {
      result = false;
      break;
    }
  }
  return result;
}

std::string format (const std::string &word)
{
  return word.substr (1, 3) + "-" + word.substr (4,6) + "-" + word.substr (7, 9);
}

int main ()
{
  std::string line, word;
  std::string temp;
  std::vector<PersonInfo> people;
  std::istringstream record;
  std::ifstream input ("eight.txt");
  while (getline(input, line))
  {
    PersonInfo info;
    record.str(line);
    record >> info.name;
    while (record >> word)
      info.phones.push_back(word);
    people.push_back(info);
    record.clear();
  }
  for (const auto &entry :people)
  {
    std::ostringstream formatted, badNums;
    for (const auto &nums: entry.phones)
    {
      if (!valid(nums))
      {
        badNums << " " << nums;
      }
      else
        formatted << " " << format (nums);
    }
    if (badNums.str().empty())
      std::cout << entry.name << formatted.str() << std::endl;
    else
      std::cerr << "input error: " << entry.name <<
        " invalid number (s) " << badNums.str() << std::endl;
  }
  return 0;
}
