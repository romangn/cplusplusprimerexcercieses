#include <iostream>
#include <sstream>
#include <vector>

struct PersonInfo
{
  std::string name;
  std::vector<std::string> phones;
};

int main ()
{
  std::string line, word;
  std::string temp;
  std::vector<PersonInfo> people;
  std::istringstream record;
  while (getline(std::cin, line))
  {
    PersonInfo info;
    record.str(line);
    record >> info.name;
    while (record >> word)
      info.phones.push_back(word);
    people.push_back(info);
    record.clear();
  }
  for (std::vector<PersonInfo>::const_iterator it = people.cbegin(); it != people.cend(); ++it)
  {
    std::cout << it->name;
    for (auto el : it->phones)
      std::cout << el << std::endl;
  }
  return 0;
}
