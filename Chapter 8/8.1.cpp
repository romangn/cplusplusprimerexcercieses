#include <iostream>

std::istream &read (std::istream &is)
{
  std::string word;
  std::string test;
  while (!is.eof())
  {
    is >> test;
    word += " ";
    word += test;
  }
  std::cout << word << std::endl;
  std::cout << is.rdstate() << std::endl;
  is.clear();
  std::cout << is.rdstate() << std::endl;
  return is;
}

int main ()
{
  read(std::cin);
  return 0;
}
