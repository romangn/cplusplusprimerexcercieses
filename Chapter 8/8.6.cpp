#include <iostream>
#include <fstream>

class Sales_data
{
public:
  Sales_data (std::string s, int volume, double d): bookNo (s), booksSold(volume),
    revenue (d){}
  Sales_data () : Sales_data ("", 0, 0.0) {}
  Sales_data (std::ifstream &is): Sales_data ()
  {
    read(is, *this);
  }
  std::ifstream &read (std::ifstream &is, Sales_data &data)
  {
    is >> data.bookNo >> data.booksSold >> data.revenue;
    return is;
  }
  std::string isbn () const
  {
    return bookNo;
  }
  int getVolume () const
  {
    return booksSold;
  }
  double getRevenue () const
  {
    return revenue;
  }

  Sales_data &combine (const Sales_data &data)
  {
    booksSold += data.booksSold;
    revenue += data.revenue;
    return *this;
  }

  std::ostream &print (std::ofstream &os, Sales_data &data);

private:
  std::string bookNo;
  unsigned int booksSold = 0;
  double revenue = 0.0;
};

std::ofstream &print (std::ofstream &os, Sales_data &data)
{
  os << data.isbn() << " " << data.getVolume() << " " << data.getRevenue() << std::endl;
  return os;
}

int main (int argc, char* argv [])
{
  std::ofstream output (argv [2], std::ofstream::app);
  std::ifstream input(argv[1]);
  Sales_data total (input);
  if (!total.isbn().empty())
  {
    while (input)
    {
      Sales_data trans (input);
      if (trans.isbn() == total.isbn())
        total.combine(trans);
      else
      {
        print(output, total);
        total = trans;
      }
    }
    print (output, total);
  }
  return 0;
}
