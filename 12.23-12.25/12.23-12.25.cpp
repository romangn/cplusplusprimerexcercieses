// 12.23-12.25.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>


int main()
{
	// 12.23 a)
	const char* c1 = "Hello";
	const char* c2 = ", world!";
	char* arr = new char[strlen("Hello, world!") + 1]();
	strcat(arr, c1);
	strcat(arr, c2);
	std::cout << arr << std::endl;
	delete[] arr;
	// 12.23 b)
	std::string s1 = "Hello";
	std::string s2 = ", world!";
	std::string* arr2 = new std::string[15]{ s1 + s2 };
	std::cout << *arr2 << std::endl;

	// 12.24 It seems to add more characters than possible. No idea why
	std::cout << "Please provide length if char array" << std::endl;
	int length;
	std::cin >> length;
	char* input = new char[length + 1] ();
	std::cin >> input;
	std::cout << input << std::endl;
	delete[] input;

	// 12.25 delete [] pa;
    return 0;
}

