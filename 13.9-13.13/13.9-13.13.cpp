// 13.9-13.13.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "HasPtr.h"
#include <vector>

struct X
{
	X() { std::cout << "X()" << std::endl; }
	X(const X&) { std::cout << "X(const X&)" << std::endl; }
	X& operator=(const X&) { 
		std::cout << "X& operator=(const X&)" << std::endl;
		return *this; }
	~X() { std::cout << "~X()" << std::endl; }
};

void hasPtrTest();
void structXTest();

int main()
{
	// 13.9 Destructor is an operation that is responsible for destruction of the object. It frees the resources
	// of the object and delete its non static data members. Destructor does not take parameters and has no return value
	// but it has a function body. Firstly, desctructor perfroms operations defined in the function body after 
	// which it reversly destroys members of the object. It is called when an object or variable goes out of scope.
	// The same also works for containers - when the container is destroyed each its element is destroyed too.
	// HOWEVER the destructor does not automatically destroy dynamically allocated memory ie int*p = new int(42)
	// , the delete has to be done in the function body. Smart pointers however does not require specific operations
	// and properly destroy itself and resource associated with it. If no specifi destructor is defined by the user,
	// the compiler will automatically create synthesized destructor that has empty function body ~Sales_Data() {}.
	// The synthesized constructor therefore will NOT destroy dynamically allocated memory (raw pointer).

	// 13.10 StrBlob SharedPtr is the only member and its counter decrements. If it is not used anywhere else,
	// it is destroyed

	// 13.10 StrBlobPtr counter decrements and if it goes to zero - the data it is responsible for is destroyed,
	// weak_ptr is destroyed (but not the data that it points to) and curr is destroyed too
	
	// 13.11 
	hasPtrTest();

	// 13.12 4: 1) const Sales_data *trans - thr pointer itself is destroyed, not the value it points to
	// 2) Sales_data accum is destroyed
	// 3) item 1 is destroyed
	// 4) item 2 is destroyed
	// 5) bool is destoyred, the copy is made and passed

	// 13.13
	structXTest();
    return 0;
}

void hasPtrTest()
{
	HasPtr ptr;
}

void structXTest()
{
	// Constructor
	std::cout << "Constructor operations: " << std::endl;
	X x; 
	// Copy Constructor
	std::cout << "Copy constructor operations: " << std::endl;
	X x2(x);
	X x3 = x2;
	std::cout << "Copy constructor operations to a vector: " << std::endl;
	std::vector<X> xVector {X(), X()}; 
	std::cout << "Resizing vector: " << std::endl;
	xVector.reserve(30);
	// Copy assignment
	std::cout << "Copy assignment operations on an existing elements: " << std::endl;
	x3 = x;
	std::cout << "Copy assignment operations within a vector on an existing element: " << std::endl;
	xVector[0] = x3;
	std::cout << "Creating new values in vector using copy constructor" << std::endl;
	xVector.push_back(X(x));
	xVector.push_back(X(x));
	std::cout << "Destroying everything, even elements within vector and vector: " << std::endl;
}
