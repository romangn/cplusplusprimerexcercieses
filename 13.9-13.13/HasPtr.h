#pragma once
#include <iostream>
#include <string>

class HasPtr
{
public:
	HasPtr(const std::string &s = std::string()) :
		ps(new std::string(s)), i(0) { }
	~HasPtr() 
	{
		if (ps)
		{
			delete ps;
		}
		std::cout << "Deleted" << std::endl;
	}
	HasPtr(const HasPtr&);
	HasPtr& operator= (const HasPtr&);
	std::string getValue() { return *ps; };
private:
	std::string* ps;
	int i;
};