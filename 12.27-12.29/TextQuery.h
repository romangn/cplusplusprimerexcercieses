#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
#include <fstream>
#include <sstream>
#include <memory>

class TextQuery
{
public:
	TextQuery(std::ifstream& file) { processFile(file); };
	std::shared_ptr<std::map<std::string, std::set<int>>> getResults();
private:
	std::vector<std::string> input;
	std::shared_ptr<std::map<std::string, std::set<int>>> wordAndLines = std::make_shared<std::map<std::string, std::set<int>>>();
	void processFile(std::ifstream&);
	void mapFile(std::shared_ptr<std::map<std::string, std::set<int>>> ptr);
};
