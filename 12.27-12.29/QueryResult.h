#pragma once
#include <memory>
#include <map>
#include <set>
#include <iostream>
#include <string>

class QueryResult
{
public:
	QueryResult(std::shared_ptr<std::map<std::string, std::set<int>>> ptr) { displayQuery(ptr); };
	void displayQuery(std::shared_ptr<std::map<std::string, std::set<int>>> ptr);
private:
	void printStuff(const std::string& s, std::shared_ptr<std::map<std::string, std::set<int>>> ptr);
};