#include "stdafx.h"
#include "QueryResult.h"

void QueryResult::displayQuery(std::shared_ptr<std::map<std::string, std::set<int>>> ptr)
{
	do 
	{
		std::cout << "enter word to look for or q to quit: ";
		std::string s;
		if (!(std::cin >> s) || s == "q") break;
		printStuff(s, ptr);
	} 
	while (true);
}

void QueryResult::printStuff(const std::string& s, std::shared_ptr<std::map<std::string, std::set<int>>> ptr)
{
	auto it = (*ptr).find(s);
	if (it != (*ptr).cend())
	{
		std::cout << "The word is " << it->first << " and it occurs on lines: ";
		for (auto& i : it->second)
		{
			std::cout << i << " ";
		}
		std::cout << std::endl;
	}
	else { std::cout << "The word " << s << " does not exist in text" << std::endl; }
}