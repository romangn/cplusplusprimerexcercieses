#include "stdafx.h"
#include "TextQuery.h"

void TextQuery::processFile(std::ifstream& inputFile)
{
	std::string s;
	while (std::getline(inputFile, s))
	{
		input.push_back(s);
	}
	inputFile.close();
	mapFile(wordAndLines);
}

void TextQuery::mapFile(std::shared_ptr<std::map<std::string, std::set<int>>> ptr)
{
	for (int i = 0; i < input.size(); i++)
	{
		std::string word;
		std::istringstream is(input[i]);		
		while (is >> word)
		{		
			(*ptr)[word].insert(i);
			//wordAndLines[word].insert(i);
		}
	}
}

std::shared_ptr<std::map<std::string, std::set<int>>> TextQuery::getResults()
{
	return wordAndLines;
}