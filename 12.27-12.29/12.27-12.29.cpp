// 12.27-12.29.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <map>
#include <set>
#include <sstream>
#include <fstream>
#include "TextQuery.h"
#include "QueryResult.h"


std::vector<std::string> processFile(std::ifstream& inputFile);
std::map<std::string, std::set<int>> mapFile(std::vector<std::string>& input);
void printStuff(const std::string& s, std::map<std::string, std::set<int>>& map);

int main()
{
	// 12.27
	//std::ifstream file("input.txt");
	//TextQuery tx(file);
	//QueryResult qr(tx.getResults());
	// 12.28
	std::ifstream file("input.txt");
	auto input = processFile(file);
	auto theMap = mapFile(input);
	// 12.29 The difference between while and do while is that do while supposed to do the action at least one
	// time even if condition is not satisfied while normal while loop supposed to do the action until condtion is met
	// or until particular value is reached.
	// In this case it doesnt matter though normal while loop gives better readibility 
	do 
	{
		std::cout << "Enter word to look for or q to quit: ";
		std::string s;
		if (!(std::cin >> s) || s == "q") break;
		printStuff(s, theMap);
	} 
	while (true);
    return 0;
}


std::vector<std::string> processFile(std::ifstream& inputFile)
{
	std::vector<std::string> input;
	std::string s;
	while (std::getline(inputFile, s))
	{
		input.push_back(s);
	}
	inputFile.close();
	return input;
}

std::map<std::string, std::set<int>> mapFile(std::vector<std::string>& input)
{
	std::map<std::string, std::set<int>> wordAndLines;
	for (int i = 0; i < input.size(); i++)
	{
		std::string word;
		std::istringstream is(input[i]);
		while (is >> word)
		{
			wordAndLines[word].insert(i);
		}
	}
	return wordAndLines;
}

void printStuff(const std::string& s, std::map<std::string, std::set<int>>& map)
{
	auto it = map.find(s);
	if (it != map.cend())
	{
		std::cout << "The word is " << it->first << " and it occurs on lines: ";
		for (auto& i : it->second)
		{
			std::cout << i << " ";
		}
		std::cout << std::endl;
	}
	else { std::cout << "The word " << s << " does not exist in text" << std::endl; }

}

