#pragma once
#include <ostream>
#include <memory>
#include <vector>
#include <set>
#include <algorithm>
#include <string>

class StrBlob;
class QueryResult
{
	using line_no = std::vector<std::string>::size_type;
	friend std::ostream& print(std::ostream&, const QueryResult&);
	friend 	std::string make_plural(size_t size, const std::string& word,
		const std::string& end);
public:
	QueryResult(std::string s,
		std::shared_ptr<std::set<line_no>> p,
		std::shared_ptr<StrBlob> f) :
		sought(s), lines(p), sharedPtrToFileStrBlob(f) {}
	std::set<line_no>::iterator begin();
	std::set<line_no>::iterator end();
	std::shared_ptr<std::vector<std::string>> getFile();
private:
	std::string sought;
	std::shared_ptr<std::set<line_no>> lines;
	std::shared_ptr<StrBlob> sharedPtrToFileStrBlob;
	std::shared_ptr<std::vector<std::string>> file;
};