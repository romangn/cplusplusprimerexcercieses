#include "stdafx.h"
#include "Employee.h"

Employee::Employee()
{
	counter++;
	uniqueIdentifier = counter;
}

Employee::Employee(const std::string s): name(s)
{
	counter++;
	uniqueIdentifier = counter;
}

Employee::Employee(const Employee& emp)
{
	name = emp.name;
	counter++;
	uniqueIdentifier = counter;
}

Employee& Employee::operator=(const Employee& emp)
{
	name = emp.name;
	return *this;
}

void Employee::getDetails()
{
	std::cout << "The name of the employee is " << name << " and his/her unique id is " << uniqueIdentifier << " ." << std::endl;
}

int Employee::counter = 0;
