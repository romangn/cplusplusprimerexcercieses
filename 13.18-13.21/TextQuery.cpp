#include "stdafx.h"
#include "TextQuery.h"
#include "QueryResult.h"
#include "StrBlob.h"

TextQuery::TextQuery(std::ifstream &is) : file(new std::vector<std::string>), sharedPtrToFileStrBlob(new StrBlob)
{
	std::string text;
	while (std::getline(is, text))
	{
		sharedPtrToFileStrBlob->pushBack(text);
		int n = sharedPtrToFileStrBlob->size() - 1;
		std::istringstream line(text);
		std::string word;
		while (line >> word)
		{
			auto& lines = wm[word];
			if (!lines)
				lines.reset(new std::set<line_no>);
			lines->insert(n);
		}
	}
}

QueryResult TextQuery::query(const std::string& sought) const
{
	static std::shared_ptr<std::set<line_no>> nodata(new std::set<line_no>);
	auto loc = wm.find(sought);
	if (loc == wm.end())
		return QueryResult(sought, nodata, sharedPtrToFileStrBlob);
	else
		return QueryResult(sought, loc->second, sharedPtrToFileStrBlob);
}