#pragma once
#include <vector>
#include <string>
#include <memory>

class  StrBlob
{
public:

	typedef std::vector<std::string>::size_type sizeType;
	StrBlob();
	StrBlob(std::initializer_list<std::string> li);
	sizeType size() const;
	bool empty() const;
	void pushBack(const std::string& s);
	void popBack();
	std::string& back() const;
	std::string& front() const;
	std::vector<std::string>::iterator getData();

private:
	std::shared_ptr<std::vector<std::string>> data;
	void check(sizeType i, const std::string& message) const;
};
