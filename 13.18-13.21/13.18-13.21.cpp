// 13.18-13.21.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Employee.h"


int main()
{
	// 13.18 - 13.19 Yes, imho it does need copy constructor and copy assignment to ensure that id numbers would stay
	// the same. The names can be changed though
	Employee emp1("Dave Johnson");
	Employee emp2("Bob bobertson");
	emp1.getDetails();
	emp2.getDetails();
	Employee emp3(emp1);
	emp3.getDetails();
	emp3 = emp2;
	emp3.getDetails();
    return 0;
}

