#pragma once
#include <string>
#include <iostream>

class Employee
{
public:
	Employee();
	Employee(const std::string);
	Employee(const Employee&);
	Employee& operator=(const Employee&);
	void getDetails();
private:
	std::string name;
	int uniqueIdentifier;
	static int counter;
};
