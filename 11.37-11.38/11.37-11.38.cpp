// 11.37-11.38.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <unordered_map>

std::unordered_map<std::string, std::string> buildMap(std::ifstream& mapFile);
const std::string& transform(const std::string& s, const std::unordered_map<std::string, std::string>& map);
void transformWord(std::ifstream& mapFile, std::ifstream& inputFile);

int main()
{
	// 11.37
	// Unordered containers may lead to a faster performance since they use hash to calulate positions of elements in the containter.
	// However it is necessary to write hash and equality operations for own types.
	// Ordered containters are slower, however the elements within those containers are sorted depending on its key.

	// 11.38
	// Word counting
	std::unordered_map<std::string, size_t> wordCounting;
	std::string word;
	while (std::cin >> word)
	{
		++wordCounting[word];
	}
	for (const auto& s : wordCounting)
	{
		std::cout << s.first << " occurs " << s.second << ((s.second > 1) ? " times. " : " time.") << std::endl;
	}

	// Word transformation
	std::ifstream rulesFile;
	std::ifstream inputFile;
	rulesFile.open("rules.txt");
	inputFile.open("input.txt");
	transformWord(rulesFile, inputFile);
	rulesFile.close();
	inputFile.close();
    return 0;
}

std::unordered_map<std::string, std::string> buildMap(std::ifstream& mapFile)
{
	std::unordered_map<std::string, std::string> returnedMap;
	std::string key;
	std::string value;
	while (mapFile >> key && std::getline(mapFile, value))
	{
		if (value.size() > 1)
		{
			returnedMap[key] = value.substr(1);
		}
		else
			throw std::runtime_error("No value found for the key " + key);
	}
	return returnedMap;
}

const std::string& transform(const std::string& s, const std::unordered_map<std::string, std::string>& map)
{
	auto it = map.find(s);
	if (it != map.cend())
	{
		return it->second;
	}
	else
		return s;
}

void transformWord(std::ifstream& mapFile, std::ifstream& inputFile)
{
	auto map = buildMap(mapFile);
	std::string text;
	while (std::getline(inputFile, text))
	{
		std::istringstream stream(text);
		std::string word;
		bool firstWord = true;
		while (stream >> word)
		{
			if (firstWord)
			{
				firstWord = false;
			}
			else
			{
				std::cout << " ";
			}
			std::cout << transform(word, map);
		}
		std::cout << std::endl;
	}
}
