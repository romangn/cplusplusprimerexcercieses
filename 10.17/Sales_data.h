#pragma once
#include <iostream>
#include <string>

class Sales_data 
{
public:
	Sales_data(const std::string& s) : bookNo(s) {};
	Sales_data(const std::string&s, unsigned n, double p) :
		bookNo(s), unitsSold(n), revenue(n * p) {};

	std::string isbn() const { return bookNo; };
	
private:
	std::string bookNo;
	unsigned unitsSold = 0;
	double revenue = 0.0;
};
