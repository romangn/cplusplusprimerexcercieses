// 10.17.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Sales_data.h"
#include <algorithm>
#include <vector>
#include <string>


int main()
{
	std::vector<Sales_data> data = { Sales_data("xcvcfsdfsa", 20, 50), Sales_data("asdasdasd", 40, 55),
		Sales_data("xcvxcvcxv", 4, 5), Sales_data("sdgzdgsdgsfzfs", 4, 5), Sales_data("zvxfsfs", 4, 5),
		Sales_data("12345", 4, 5) };
	std::cout << "Prior to sort";
	std::cout << std::endl;
	for (Sales_data d : data) { std::cout << d.isbn() << std::endl; };
	std::sort(data.begin(), data.end(), [](Sales_data& s1, Sales_data& s2) { return s1.isbn().size() < s2.isbn().size(); });
	std::cout << "After sort";
	std::cout << std::endl;
	for (Sales_data d : data) { std::cout << d.isbn() << std::endl; };
    return 0;
}

