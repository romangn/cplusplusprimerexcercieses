// 10.21.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>


int main()
{
	int i = 10;
	auto f = [i]() mutable { while (i > 0) { --i; } if (i == 0) { return true; } return false; };
	std::cout << f() << std::endl;
    return 0;
}

