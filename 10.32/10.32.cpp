// 10.32.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include "Sales_item.h"

void doStuff()
{
	std::vector<Sales_item> items;
	std::istream_iterator<Sales_item> salesIterator(std::cin), eof;
	while (salesIterator != eof)
	{
		items.push_back(*salesIterator++);
	}
}

int main()
{
	//Sales_item total;
	//if (std::cin >> total)
	//{
	//	Sales_item trans;
	//	while (std::cin >> trans)
	//	{
	//		if (total.isbn() == trans.isbn()) { total += trans; }
	//		else
	//		{
	//			std::cout << total << std::endl;
	//			total = trans;
	//		}
	//	}
	//	std::cout << total << std::endl;
	//}
	//else
	//{
	//	std::cerr << "No data" << std::endl;
	//	return -1;
	//}
	std::vector<Sales_item> items;
	std::istream_iterator<Sales_item> salesIterator(std::cin), eof;
	while (salesIterator != eof)
	{
		items.push_back(*salesIterator++);
	}
	std::sort(items.begin(), items.end(), compareIsbn);
	for (auto beg = items.cbegin(), end = beg; beg != items.cend(); beg = end)
	{
		end = std::find_if(beg, items.cend(), [beg](const Sales_item& item) {
			return item.isbn() != beg->isbn();
		});
		std::cout << std::accumulate(beg, end, Sales_item(beg->isbn())) << std::endl;
	}
    return 0;
}

