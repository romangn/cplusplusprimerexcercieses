// 10.18.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

std::string make_plural(size_t ctr, const std::string& word,
	const std::string& ending);
void elimDumps(std::vector<std::string>& words);
void biggiesStable(std::vector<std::string>& words,
	std::vector<std::string>::size_type sz);
void biggiesPartition(std::vector<std::string>& words,
	std::vector<std::string>::size_type sz);

int main()
{
	std::vector<std::string> test = { "test", "tset", "dog", "cat", "fat", "CAT", "test" };
	biggiesPartition(test, 3);
    return 0;
}

std::string make_plural(size_t ctr, const std::string& word,
	const std::string& ending)
{
	return (ctr > 1) ? word + ending : word;
}

void elimDumps(std::vector<std::string>& words)
{
	std::cout << "Before the initial sort: ";
	std::cout << std::endl;
	for (auto s : words) { std::cout << s << std::endl; }
	std::sort(words.begin(), words.end());
	std::cout << "After the sort";
	std::cout << std::endl;
	for (auto s : words) { std::cout << s << std::endl; }
	auto it = std::unique(words.begin(), words.end());
	words.erase(it, words.end());
	std::cout << "Removal of duplicates";
	std::cout << std::endl;
	for (auto s : words) { std::cout << s << std::endl; }
}

void biggiesPartition(std::vector<std::string>& words,
	std::vector<std::string>::size_type sz)
{
	elimDumps(words);
	std::stable_sort(words.begin(), words.end(),
		[](const std::string s1, const std::string s2) {return s1.size() < s2.size(); });
	auto it = std::partition(words.begin(), words.end(), [sz](const std::string& s1) { return s1.size() > sz; });
	auto count = words.end() - it;
	std::cout << count << " " << make_plural(count, "word", "s") << " of length " << sz << " or longer" << std::endl;
	std::for_each(words.begin(), words.end(), [](const std::string s1) { std::cout << s1 << " "; });
	std::cout << std::endl;
}

void biggiesStable(std::vector<std::string>& words,
	std::vector<std::string>::size_type sz)
{
	elimDumps(words);
	std::stable_sort(words.begin(), words.end(),
		[](const std::string s1, const std::string s2) {return s1.size() < s2.size(); });
	auto it = std::stable_partition(words.begin(), words.end(), [sz](const std::string& s1) { return s1.size() > sz; });
	auto count = words.end() - it;
	std::cout << count << " " << make_plural(count, "word", "s") << " of length " << sz << " or longer" << std::endl;
	std::for_each(words.begin(), words.end(), [](const std::string s1) { std::cout << s1 << " "; });
	std::cout << std::endl;
}

