#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;

int main ()
{
    vector <int> v1 = {1, 2, 3, 4, 5, 6, 7,
        8, 9, 10};
    for (auto it = v1.begin();it != v1.end();++it)
    {
        (*it % 2 == 0) ? *it *= 2 : *it = *it;
    }
    for (auto it: v1)
        cout << it << endl;
    return 0;
}
