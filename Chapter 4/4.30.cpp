#include <iostream>

using std::cout;using std::endl;
using std::end;

struct Sales_data
{
    int arr [5];
    int balance = 0;
};

int main ()
{
    Sales_data data;
    int counter = 1;
    for (auto i = data.arr;i != end(data.arr);++i)
    {
        *i = counter;
        ++counter;
    }
    int a, b;
    a = 5;
    b = 7;
    cout << (sizeof a) + b << endl;
    cout << sizeof(data.arr[1]) << endl;
    cout << (sizeof (a) < b) << endl;
    return 0;
}

int f ()
{
    int a, b;
    a += 1;
    return a;
}
