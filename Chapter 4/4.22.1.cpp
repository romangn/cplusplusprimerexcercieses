#include <iostream>

using std::cin; using std::cout; using std::endl;

int main ()
{
    unsigned int grade;
    cin >> grade;
    while (grade > 100)
        cin >> grade;
    if (grade >= 90)
        cout << "High pass" << endl;
    else
        if (grade >= 60 && grade <= 75)
            cout << "Low pass" << endl;
    else
        if (grade >= 60)
            cout << "Pass" << endl;
    else
        cout << "Fail" << endl;
    return 0;
}
