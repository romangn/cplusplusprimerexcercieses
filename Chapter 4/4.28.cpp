#include <iostream>

using std::cout; using std::endl;

int main ()
{
    cout << sizeof (char) << endl;
    cout << sizeof (unsigned char) << endl;
    cout << sizeof (signed char) << endl;
    cout << sizeof (int) << endl;
    cout << sizeof (double) << endl;
    cout << sizeof (short int) << endl;
    cout << sizeof (size_t) << endl;
    cout << sizeof (float) << endl;
    return 0;
}
