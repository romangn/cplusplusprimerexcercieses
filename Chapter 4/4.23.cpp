#include <iostream>
#include <string>

using std::cout; using std::endl;
using std::string;

int main ()
{
    string s = "word";
    //So its actually checks first if the last letter is 'S'
    //and only then decides what to add and not adds and then
    //checks as I thought at first
    string p1 = s + (s[s.size() - 1] == 's' ? "" : "s");
    cout << p1 << endl;
    return 0;
}
