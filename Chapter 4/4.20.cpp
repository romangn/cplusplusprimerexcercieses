#include <iostream>
#include <vector>
#include <string>

using std::cout; using std::endl;
using std::vector; using std::string;

int main ()
{
    vector <string> s1 = {"This", "is", "a", "test"};
    vector<string>::iterator iter = s1.begin();
    cout << iter++ -> empty() << endl;
    return 0;
}
