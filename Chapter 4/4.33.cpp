#include <iostream>

using std::cout; using std::endl;

int main ()
{
    int x, y, z;
    x = 1, y = 1;
    z = 10;
    z = 10 ? ++x, ++y : --x, --y;
    cout << "x is "<< x << " and y is " << y << endl;
    return 0;
}
