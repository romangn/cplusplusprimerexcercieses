#include <iostream>

using std::cin; using std::cout; using std::endl;

int main ()
{
    unsigned int grade;
    cin >> grade;
    while (grade > 100)
        cin >> grade;
    cout << ((grade >= 90) ? "High pass"
                : (grade >= 60 && grade <= 75) ? "Low pass"
                : (grade < 60) ? "Fail" : "Pass")
                << endl;
    return 0;
}
