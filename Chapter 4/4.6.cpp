#include <iostream>

using std::cin; using std::cout; using std::endl;

int main ()
{
    unsigned int i;
    if (cin >> i)
    {
        if (i % 2 == 0)
            cout << "Even" << endl;
        else
            cout << "Odd" << endl;
    }
    for (int i = 1; i <= 100;++i)
    {
        if (i % 2 == 0)
            cout << i << " is Even" << endl;
        else
            cout << i << " is Odd" << endl;
    }
    return 0;
}
