#include <iostream>
#include <vector>

using std::cout; using std::endl;
using std::vector;

int main ()
{
    vector <int> v = {10, 9, 8, 7, 6, 5};
    auto pbeg = v.begin();
    while (pbeg != v.end() && *pbeg >= 0)
        cout << *++pbeg << endl;
    return 0;
}
