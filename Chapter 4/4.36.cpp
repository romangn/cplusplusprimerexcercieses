#include <iostream>

using std::cout; using std::endl;

int main ()
{
    int i = 5;
    double d = 2.5;
    i *= static_cast <double>(d);
    cout << i << endl;
    return 0;
}
