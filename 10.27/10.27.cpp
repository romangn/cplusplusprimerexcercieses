// 10.27.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <algorithm>
#include <list>


int main()
{
	std::vector<int> values = { 1, 2, 3, 1, 4, 2, 5, 6, 7, 8, 1, 2, 3 };
	std::list<int> listValues;
	std::sort(values.begin(), values.end());
	std::unique_copy(values.begin(), values.end(), std::back_inserter(listValues));
	for (auto it : listValues) { std::cout << it << std::endl; }
    return 0;
}

