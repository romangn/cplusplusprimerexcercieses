// 10.20.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <algorithm>


int main()
{
	std::vector<int> coll = { 1, 2, 3, 4, 6, 9, 8, 5, 3, 10, 99};
	int wc = std::count_if(coll.begin(), coll.end(),
		[](int n) {return n > 6; });
	std::cout << wc << std::endl;
    return 0;
}

