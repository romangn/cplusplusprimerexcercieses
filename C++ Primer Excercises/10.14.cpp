// C++ Primer Excercises.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>


int main()
{
	auto it = [](int i, int j) {return i + j; };
	std::cout << it(5, 6) << std::endl;
    return 0;
}

