// 10.15.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>


int main()
{
	int i = 42;
	auto value = [i](int n) {return i + n; };
	std::cout << value(5) << std::endl;
    return 0;
}


